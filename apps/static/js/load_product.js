function loadProduct() {
  $.ajax({
    url: window.urls.loadProducto,
    data: {
      "subproduct_id": $("#id_subproduct").val()
    },
    dataType: "json",
    type: "GET",
    success: function (data) {
      $("#id_product").val(data[0]).trigger('change');
      $.ajax({
        url: window.urls.subproductsApi,
        data: {
          "id": data[0],
        },
        type: "GET",
        success: function (data) {
          var past_value = $("#id_subproduct").val();
          if (data.status === 200) {
            $("#id_subproduct").empty();
            $("#id_subproduct").select2({
              data: data.data
            }).val(past_value).trigger("change");
          }
        }
      })
    },
    complete: function (xhr, textStatus) {
    },
    error: function (xmlhttprequest, textstatus, message) {
    }
  });
}
$(function () {
  loadProduct();
});
