function agregaUrl(id, url) {
  $(id).attr("href", url);
}

function agregaAction(id, action) {
  $(id).attr("action", action);
}

function initButtonsActivity(row_id) {
  agregaAction("#remover-file", window.urls.removeFile.replace(999, row_id));
}

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie !== '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

$(function () {
  var rowId;
  var tablaActivity = $("#attachedfiles").DataTable({
    columns: [
      {"width": "2%"},
      {"width": "12%"},
      {"width": "18%"},
      {"width": "12%"},
      {"width": "35%"},
      {"width": "21%"}
    ],
    fixedColumns: true,
    columnDefs: [{
      orderable: false,
      className: "select-checkbox",
      targets: 0
    }],
    select: true,
    loadingRecords: "Cargando actividades ...",
    emptyTable: "No se encontraron actividades",
    language: {
      emptyTable: "No se encontraron elementos"
    },
    info: false
  });
  $("#attachedfiles").on("click", "tr", function () {
    initButtonsActivity($(this).attr("id"));
    $("#exp_ids").val($(this).attr("id"));
  });
  //$.fn.dataTable.ext.errMode = 'throw';
  $("#btnRemoverConfirmacionFile").click(function () {
    initButtonsActivity($("#exp_ids").val());
    $("#remover-file").submit();
  });
  $.fn.editable.defaults.mode = 'inline';
  $.fn.editableform.buttons =
      '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
      '<i class="fa fa-check"></i>' +
      '</button>' +
      '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
      '<i class="fa fa-times"></i>' +
      '</button>';

});