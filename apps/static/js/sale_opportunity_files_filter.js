$(function () {
    var rowId;
    var tablaFiles = $("#files").DataTable({
        columns: [
            {"width": "2%"},
            {"width": "8%"},
            {"width": "40%"},
            {"width": "20%"},
            {"width": "20%"},
            {"width": "10%"},
        ],
        responsive: {
            details: true
        },
        columnDefs: [{
            orderable: false,
            className: "select-checkbox",
            targets: 0
        }],
        PaginationType: "bootstrap",
        pageLength: 8,
        searching: false,
        bLengthChange: false,
        paging: true,
        bFilter: false,
        info: false,
        select: true,
        language: {
            paginate: {
                previous: "<",
                next: ">",
                first: "|<",
                last: ">|"
            },
            loadingRecords: "Cargando files ...",
            emptyTable: "No se encontraron documentos"
        },
        order: [[1, "desc"]],
        sDom: "Rlfrtip",
        initComplete: function () {
            //this.api().row( {order:  'current'}, 0).select();
        }
    });
});