jQuery.fn.exists = function () {
    return this.length > 0;
};

function input_number(input, size) {
    if (input.exists()) {
        input.keypress(function (e) {
            var key = window.event ? e.keyCode : e.which;

            if ($(this).val().length === size) {
                return e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39;
            } else {
                if (e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39) {
                    return true;
                }
                else if (key < 48 || key > 57) {
                    return false;
                }
                else {
                    return true;
                }
            }
        });

    }
}

function input_number(input, size, inputSearch) {
    if (input.exists()) {
        input.keypress(function (e) {
            var key = window.event ? e.keyCode : e.which;
            if ($(this).val().length === size) {
                if (e.charCode === 13) {
                    inputSearch.trigger("click");
                }
                return e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39;
            } else {
                if (e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39) {
                    return true;
                }
                else if (key < 48 || key > 57) {
                    return false;
                }
                else {
                    return true;
                }
            }
        });
    }
}

function input_max(input, max) {
    if (input.exists()) {
        input.change(function (e) {
            if (parseInt($(this).val()) > max) {
                $(this).val(max);
            }
        });
    }
}

$(function () {
    input_number($("#id_estimated_time"), 2);
    input_number($("#id_revenue"), 12);
    input_number($("#id_ganancia"), 12);
    var tipoAdjunto = $('[id ^=id_form-][id $=-type]');

});


// Numeric only control handler
jQuery.fn.IngresoSoloNumeros =
    function () {
        return this.each(function () {
            $(this).keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    $("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });
        });
    }