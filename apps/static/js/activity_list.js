function agregaUrl(id, url) {
    $(id).attr("href", url);
}

function agregaAction(id, action) {
    $(id).attr("action", action);
}

function initButtonsActivity(row_id) {
    agregaAction("#remover-activity", window.urls.removeActivity.replace(999, row_id));
    agregaUrl("#editar-activity", window.urls.editActivity.replace(999, row_id));
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(function () {
    //var editableActive = false;
    //Setup - add a text input to each footer cell
    // var contador = 0;
    // $('#activities thead tr#tr_search th').each(function () {
    //     if (contador != 0) {
    //         var title = $(this).text();
    //         $(this).html('<input type="text" placeholder="' + title + '" />');
    //     }
    //     contador++;
    // });
    //$("#op-check").css('display', 'none');

    var rowId;
    var tablaActivity = $("#activities").DataTable({
        columns: [
            {"width": "2%"},
            {"width": "53%"},
            {"width": "12%"},
            {"width": "12%"},
            {"width": "6%"},
            {"width": "10%"},
            {"width": "5%"},
        ],
        fixedColumns: true,
        columnDefs: [{
            orderable: false,
            className: "select-checkbox",
            targets: 0
        }],
        // sPaginationType: "bootstrap",
        // searching: false,
        // bLengthChange: false,
        // paging: false,
        //bFilter: false,
        // info: false,
        select: true,
        // language: {
        //   paginate: {
        //    previous: "<",
        //    next: ">",
        //    first: "|<",
        //   last: ">|"
        // },
        loadingRecords: "Cargando actividades ...",
        emptyTable: "No se encontraron actividades",
        // },
        //  order: [[1, "desc"]],
        //  sDom: "Rlfrtip",
        //   initComplete: function() {

        //this.api().row( {order:  'current'}, 0).select();
//   }

    });
    // tablaActivity.columns().eq(0).each(function (colIdx) {
    //     $('input', tablaActivity.column(colIdx).header()).on('keyup change', function () {
    //         tablaActivity
    //             .column(colIdx)
    //             .search(this.value)
    //             .draw();
    //     });
    // });
    $("#activities").on("click", "tr", function () {
        initButtonsActivity($(this).attr("id"));
        $("#exp_ids").val($(this).attr("id"));
    });
    //$.fn.dataTable.ext.errMode = 'throw';
    $("#btnRemoverConfirmacionActivity").click(function () {
        initButtonsActivity($("#exp_ids").val());
        $("#remover-activity").submit();
    });
    $.fn.editable.defaults.mode = 'inline';
    $.fn.editableform.buttons =
        '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
        '<i class="fa fa-check"></i>' +
        '</button>' +
        '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
        '<i class="fa fa-times"></i>' +
        '</button>';
    // $("#movimientos a.ingreso-parcial").editable({
    //   url: '/post',
    //   type: 'text',
    //   pk: 1,
    //   name: 'xxx',
    //   title: 'Enter username'
    // });

    // $("#movimientos").on("click", "tr", function () {
    //   var $this;
    //   $this = $(this);
    //   console.log(editableActive);
    //   if (editableActive === false) {
    //     if ($this.hasClass("selected")) {
    //       $(this).removeClass("selected");
    //     } else {
    //       $(this).addClass("selected");
    //     }
    //   }
    // });


});