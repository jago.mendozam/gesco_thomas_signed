/**
 * Created by jago on 12/09/18.
 */
$(document).ready(function () {
    // Setup - add a text input to each footer cell
    $('#clients thead tr#tr_search th').each(function () {
        var title = $(this).text();
        $(this).html('<input style="width: 100%;" type="text" placeholder="' + title + '" />');
    });
    $("#th-buttons").css('display', 'none');
    var table = $('#clients').DataTable({
        columnDefs: [
            {"width": "5%"},
            {"width": "5%"},
            {"width": "25%"},
            {"width": "10%"},
            {"width": "20%"},
            {"width": "5%"},
            {"width": "5%"},
            {"width": "25%"},
        ],
        fixedColumns: true,
        info: false,
        ordering: false,
        sPaginationType: "full_numbers",
        bLengthChange: false,
        language: {
            paginate: {
                previous: "<",
                next: ">",
                first: "|<",
                last: ">|"
            },
            loadingRecords: "Cargando...",
            emptyTable: "No se encontraron elementos"
        },
    });
    //search
    table.columns().eq(0).each(function (colIdx) {
        $('input', table.column(colIdx).header()).on('keyup change', function () {
            table
                .column(colIdx)
                .search(this.value)
                .draw();
        });
    });

    $("#btnRemoverConfirmacion").click(function () {
        initBotones($("#selectedRow").val());
        $("#remover-cliente").submit();
    });
});

function agregaAction(id, action) {
    $(id).attr("action", action);
}

function initBotones(row_id) {
    agregaAction("#remover-cliente", window.urls.removercliente.replace(999, row_id));
}