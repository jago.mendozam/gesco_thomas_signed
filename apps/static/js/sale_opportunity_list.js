function agregaUrl(id, url) {
    $(id).attr("href", url);
}

function agregaAction(id, action) {
    $(id).attr("action", action);
}

function initButtonsOpportunities(row_id) {
  agregaAction("#remover-oportunidad", window.urls.removeOpportunity.replace(999, row_id));
  agregaUrl("#editar-oportunidad", window.urls.editOpportunity.replace(999, row_id));
  agregaUrl("#activities-list", window.urls.activities.replace(999, row_id));
  agregaUrl("#attached-file-list", window.urls.attached_files.replace(999, row_id));
  agregaUrl("#ver-oportunidad", window.urls.verOportunidad.replace(999, row_id));
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(function () {
    var editableActive = false;
    //Setup - add a text input to each footer cell
    var contador = 0;
    $('#oportunidades thead tr#tr_search th').each(function () {
        if (contador != 0) {
            var title = $(this).text();
            $(this).html('<input style="width: 100%;" type="text" placeholder="' + title + '" />');
        }
        contador++;
    });
    // $("#op-check").css('display', 'none');

    var tablaOportunidad = $("#oportunidades").DataTable({

        columns: [
            {"width": "2%"},
            {"width": "8%"},
            {"width": "15%"},
            {"width": "10%"},
            {"width": "10%"},
            {"width": "15%"},
            {"width": "6%"},
            {"width": "9%"},
            {"width": "9%"},
            {"width": "8%"},
            {"width": "8%"},
        ],
        responsive: {details: true},
        columnDefs: [{
            orderable: false,
            className: "select-checkbox",
            targets: 0
        }],
        bJQueryUI: true,
        sPaginationType: "full_numbers",
        bLengthChange: false,
        info: false,
        select: true,
        ordering: false,
        language: {
            paginate: {
                previous: "<",
                next: ">",
                first: "|<",
                last: ">|"
            },
            loadingRecords: "Cargando...",
            emptyTable: "No se encontraron elementos"
        },
        // loadingRecords: "Cargando oportunidades ...",
        //  emptyTable: "No se encontraron oportunidades"
        // },
        //  order: [[1, "desc"]],
        //  sDom: "Rlfrtip",
        //   initComplete: function() {

        //this.api().row( {order:  'current'}, 0).select();
//   }
//    "scrollX":true
    });
    tablaOportunidad.columns().eq(0).each(function (colIdx) {
        $('input', tablaOportunidad.column(colIdx).header()).on('keyup change', function () {
            tablaOportunidad
                .column(colIdx)
                .search(this.value)
                .draw();
        });
    });

    var configTablaOportunidadDetalle = {
        columns: [
            {"width": "4%"},
            {"width": "13%"},
            {"width": "13%"},
            {"width": "13%"},
            {"width": "13%"},
            {"width": "13%"},
            {"width": "13%"},
            {"width": "18%"}
        ],
        responsive: {details: true},
        columnDefs: [{
            orderable: false,
            className: "select-checkbox",
            targets: 0
        }],
        bJQueryUI: true,
        sPaginationType: "full_numbers",
        iDisplayLength: 1,
        pageLength: 8,
        destroy: true,
        paging: true,
        searching: false,
        bLengthChange: false,
        bFilter: false,
        info: false,
        language: {
            paginate: {
                previous: "<",
                next: ">",
                first: "|<",
                last: ">|"
            },
            loadingRecords: "Cargando detalle de la oportunidad...",
            emptyTable: "No se encontraron elementos"
        },
        select: true,
        order: [[1, "asc"]],
        ajax: {
            type: "GET",
            url: window.urls.apiDetailOpVentas,
            data: function (d) {
                d.oportunidad_id = $("#exp_ids").val();
            },
            dataSrc: function (json) {
                $('#detalle-pagado').text(json.paid);
                $('#detalle-pendiente').text(json.pending);
                return json.data;
            }
        },
        createdRow: function (row, data, index) {
            for (var i = 0; i < data.length - 1; i++) {
                $("td", row).eq(i).attr("title", data[i]);
                if (i === data.length - 3) {
                    var $option = $("<input />")
                        .prop("type", "checkbox")
                        .prop('checked', data[i])
                        .attr("data-toggle", "toggle")
                        .attr("data-style", "android")
                        .attr("data-onstyle", "info")
                        .attr("data-size", "mini")
                        .attr("class", "select-toggle")
                        .attr("data-on", "SI")
                        .attr("data-off", "NO");

                    $(row).find("td").eq(i).text("");
                    $(row).find("td").eq(i).append($option);
                    $(row).find("td").eq(i).attr("style", "text-align:center");
                }
                if (i === 3) {
                    var $option = $("<a href='#' class='editable editable-click ingreso-parcial'>" + data[i] + "</a>");
                    $(row).find("td").eq(i).text("");
                    $(row).find("td").eq(i).append($option);
                    //$(row).find("td").eq(i).attr("style", "text-align:center");
                }
            }
        },
        rowCallback: function (row, data) {
            $(row).attr("id", data[data.length - 1]);
        },
        fnDrawCallback: function () {
            $(".select-toggle").bootstrapToggle();

            var partialRevenues = $(".ingreso-parcial");
            partialRevenues.each(function(i, el) {
                var partialRevenueElem = $(el);
                var detailId = $(this).parent().parent().closest('tr').attr('id');

                partialRevenueElem.editable({
                    disabled: partialRevenueElem.parent().parent().find('.select-toggle').prop('checked'),
                    url: window.urls.ingresoParcialDetalle,
                    send: 'always',
                    type: 'text',
                    name: 'ingreso_parcial',
                    title: 'editar ingreso parcial',
                    pk: detailId,
                    params: {
                        detalle_id: detailId,
                        partial_revenue: partialRevenueElem.text(),
                        csrfmiddlewaretoken: getCookie("csrftoken")
                    },
                    ajaxOptions: {
                        type: 'post'
                    },
                    success: function (response, newValue) {
                        $("#movimientos tbody tr#" + response[0]).find("td").eq(5).text(response[1]);
                        selectOpportunity($("#exp_ids").val());
                    },
                    error: function (response, newValue) {
                        return response.responseJSON.message;
                    },
                });

                partialRevenueElem.on('shown', function (e, reason) {
                    editableActive = true;
                    options = {
                        detalle_id: detailId,
                        partial_revenue: partialRevenueElem.text(),
                        csrfmiddlewaretoken: getCookie("csrftoken")
                    };
                    $(this).editable('option', {params: options})
                });
            });
        }
    };
    var tablaDetalleOportunidad = $("#movimientos").DataTable(configTablaOportunidadDetalle);

    function selectOpportunity(id) {
        if (id !== "tr_search") {
            initButtonsOpportunities(id);
            $("#exp_ids").val(id);
            tablaDetalleOportunidad.ajax.reload();
        } else {
            $('#exp_ids').val("");
        }
    }

    $("#oportunidades").on("click", "tr", function () {
        selectOpportunity($(this).attr("id"));
    });
    tablaDetalleOportunidad.clear().draw();
    $.fn.dataTable.ext.errMode = 'throw';
    $("#btnRemoverConfirmacionOpportunity").click(function () {
        initButtonsOpportunities($("#exp_ids").val());
        $("#remover-oportunidad").submit();
    });
    $("#movimientos").on("click", ".toggle-group", function () {
        var checkboxIsPaid = $(this).parent().children("input");

        var detailId = $(this).parent().parent().closest('tr').attr('id');
        var detailRow = $("#movimientos tr#" + detailId);
        var partialRevenue = detailRow.find(".ingreso-parcial");
        var isAlreadyPaid = checkboxIsPaid.prop("checked");

        // Si ya estaba pagado, y se cambia a No Pagado entonces habilitar y viceversa
        partialRevenue.editable('option', 'disabled', !isAlreadyPaid);

        var fecha_facturacion = detailRow.find("td").eq(7);
        $.ajax({
            url: window.urls.fechaDetalleFacturado,
            data: {
                "detalle_id": detailId,
                "csrfmiddlewaretoken": getCookie("csrftoken"),
                "is_paid": !isAlreadyPaid
            },
            dataType: "json",
            type: "POST",
            success: function (data) {
                fecha_facturacion.attr("title", data.date);
                fecha_facturacion.text(data.date);
                $('#detalle-pagado').text(data.paid);
                $('#detalle-pendiente').text(data.pending);
            },
            error: function (xmlhttprequest, textstatus, message) {
                if (xmlhttprequest.status === 400) {
                    fecha_facturacion.text(xmlhttprequest.responseJSON[0])
                } else {
                    fecha_facturacion.text(xmlhttprequest.responseJSON.detail);
                }
                // checkboxIsPaid.removeAttr("disabled").change();
                checkboxIsPaid.prop("checked", !isAlreadyPaid).change();
                setTimeout(function () {
                    fecha_facturacion.text("")
                }, 3000);
            }
        });
    });
    $.fn.editableform.template = `
    <form class="form-inline editableform">
        <div class="control-group">
             <div><div class="editable-input"></div><div class="editable-buttons"></div></div>
        </div> 
        <div class="editable-error-block"></div>
    </form>`;
    $.fn.editable.defaults.mode = 'inline';
    $.fn.editableform.buttons =
        '<button type="submit" class="btn btn-primary btn-sm editable-submit">' +
        '<i class="fa fa-check"></i>' +
        '</button>' +
        '<button type="button" class="btn btn-default btn-sm editable-cancel">' +
        '<i class="fa fa-times"></i>' +
        '</button>';

    tablaOportunidad.row(':eq(0)', {page: 'current'}).select();
    var primera_oportunidad_id = $("#oportunidades tr:first-child")[1].id;
    initButtonsOpportunities(primera_oportunidad_id);
    $("#exp_ids").val(primera_oportunidad_id);
    tablaDetalleOportunidad.ajax.reload();
});