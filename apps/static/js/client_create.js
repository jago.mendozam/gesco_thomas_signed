function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(document).ready(function () {
    $("#id_phone_1").IngresoSoloNumeros();
    $("#id_phone_1").attr('maxlength', '9');
    $("#id_phone_2").IngresoSoloNumeros();
    $("#id_phone_2").attr('maxlength', '9');
    $("#id_type_doc").select2();

    var fileTypes = [
        'jpeg',
        'png',
        'jpg',
        'xlsx',
        'xls',
        'doc',
        'docx',
        'pdf',
        'ppt',
        'pptx',
        'csv',
        'txt'
    ];
    var maxFileSize = 3145728;

    $('.custom-file-input').on('change', function () {
        console.log($(this)[0].files[0].size);
        var size = $(this)[0].files[0].size;
        var extension = $(this).val().split('.').pop();
        if ($.inArray(extension, fileTypes) === -1) {
            $(this).val("");
            if ($(this).next('.custom-file-label').hasClass("selected")) {
                $(this).next('.custom-file-label').removeClass("selected").html("");
            }
            alert("Tipo de archivo " + extension + " no valido");
        } else if (size > maxFileSize) {
            $(this).val("");
            alert("Archivo demasiado pesado, solo se acepta hasta 3 megas");
        } else {
            var fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        }
    });

    var department = $('#id_department');
    var province = $('#id_province');
    var district = $('#id_district');
    department.select2({
        width: "95%",
    });
    province.select2({
        width: "95%",
    });
    district.select2({
        width: "95%",
    });

    // By default on create, only used on edit
    var provincesEditedLoaded = true;
    var districtsEditedLoaded = true;

    if (districtId) {
        provincesEditedLoaded = false;
        districtsEditedLoaded = false;
    }

    department.on('change ', function () {
        $.ajax({
            url: window.urls.loadProvinces.replace(999, $(this).val()),
            dataType: "json",
            type: "GET",
            success: function (data) {
                var finalData = $.map(data, function (obj) {
                    obj.text = obj.name; // replace name with the property used for the text
                    delete obj.name;
                    return obj;
                });
                finalData.unshift({text: "------", id: 0});
                province.empty().select2({width: "95%", data: finalData});
                district.empty().select2({width: "95%"});
            }
        }).then(function () {
            if (!provincesEditedLoaded) {
                provincesEditedLoaded = true;
                province.val(provinceId).trigger('change');
            }
        });
    }).val(departmentId).trigger('change');

    province.on('change', function () {
        $.ajax({
            url: window.urls.loadDistricts.replace(999, $(this).val()),
            dataType: "json",
            type: "GET",
            success: function (data) {
                var finalData = $.map(data, function (obj) {
                    obj.text = obj.name; // replace name with the property used for the text
                    delete obj.name;
                    return obj;
                });
                finalData.unshift({text: "------", id: 0});
                district.empty().select2({width: "95%", data: finalData});
            }
        }).then(function () {
            if (!districtsEditedLoaded) {
                districtsEditedLoaded = true;
                district.val(districtId).trigger('change');
            }
        });
    });

    var numDoc = $("#id_num_doc");
    var typeDoc = $("#id_type_doc");
    numDoc.attr('maxlength', '11');
    numDoc.IngresoSoloNumeros();
    $("#id_type_person_sunat").addClass("inactiveLink");
    $("#row_name").hide();

    if (typeDoc.val() == 6) {
        $("#div_nombre_completo").hide();
        $("#row_name").show();
    }

    typeDoc.change(function () {
        var type_doc = $(this).val();

        if (type_doc == 1) {
            numDoc.attr('maxlength', '8');
            $("#row_name").hide();
            $("#div_nombre_completo").show();
            $("#id_name").val("");
            $("#id_type_person_sunat").val("TPN");
            $("#id_type_person_sunat").addClass("inactiveLink");
        } else if (type_doc == 6) {
            numDoc.attr('maxlength', '11');
            $("#row_name").show();
            $("#div_nombre_completo").hide();
            $("#id_first_name").val("");
            $("#id_second_name").val("");
            $("#id_paternal_surname").val("");
            $("#id_maternal_surname").val("");
            $("#id_type_person_sunat").val("TPJ");
            $("#id_type_person_sunat").addClass("inactiveLink");
        } else if (type_doc == 4 || type_doc == 7 || type_doc == 0) {
            if (type_doc == 0) {
                numDoc.attr('maxlength', '15');
            } else {
                numDoc.attr('maxlength', '12');
            }
            $("#row_name").hide();
            $("#div_nombre_completo").show();
            $("#id_name").val("");
            $("#id_type_person_sunat").val("SND");
            $("#id_type_person_sunat").removeClass("inactiveLink");
        } else if (type_doc == 0) {
            numDoc.attr('maxlength', '15');
            $("#row_name").hide();
            $("#div_nombre_completo").show();
            $("#id_name").val("");
            $("#id_type_person_sunat").val("SND");
            $("#id_type_person_sunat").removeClass("inactiveLink");
        }

        numDoc.val("");
        numDoc.focus();
    });

    // Only exists on edition
    var rucException = numDoc.val();

    numDoc.on('keyup', function(){
        if (numDoc.val().length === 11) {
            $.ajax({
                url: window.urls.validateRuc,
                data: {
                    "ruc": numDoc.val(),
                    "csrfmiddlewaretoken": getCookie("csrftoken"),
                    "exception": rucException,
                },
                dataType: "json",
                type: "POST",
                success: function (data) {
                    if (data.alreadyExisted) {
                        numDoc.parent().find('.ruc-error').show();
                    } else {
                        numDoc.parent().find('.ruc-error').hide();
                    }
                },
            });
        } else {
            numDoc.parent().find('.ruc-error').hide();
        }
    });

    if (rucEdit !== "") {
        $('#client-form').append('<input type="hidden" id="rucException" name="rucException" value="'+rucException+'"/>')
    }
});
