/**
 * Created by jago on 26/09/18.
 */
$("#id_product").on("change", function (e) {
    $.ajax({
        url: window.urls.subproductsApi,
        data: {
            "id": $(this).val(),
      },
      type: "GET",
     success:function (data) {
          if (data.status === 200){
              $("#id_subproduct").empty();
              $("#id_subproduct").select2({
                  data: data.data
              }).trigger("change");
              $("#id_subproduct").removeClass("inactiveLink");
          }
     }
    })
});
