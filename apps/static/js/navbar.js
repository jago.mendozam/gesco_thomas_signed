$(document).ready(function () {
  function FirstAllWords(str) {
    var pieces = str.split(' ')
    var result = pieces.map(function (piece) {
      return piece.charAt(0).toUpperCase() + piece.slice(1).toLowerCase()
    })
    return result.join(' ')
  }
  var nombre = $('#nav_nombre_user');
  nombre.text(FirstAllWords(nombre.text()));
});

