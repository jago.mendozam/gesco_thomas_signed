from django.contrib import admin

# Register your models here.
from .models import Client, Address, Contact


class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'type_doc', 'num_doc', 'type_person_sunat', 'client_group', 'email_fact', 'created_at', 'last_modified')

admin.site.register(Client, ClientAdmin)
admin.site.register(Address)
admin.site.register(Contact)
