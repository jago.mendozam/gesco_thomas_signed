from django.contrib import messages
from django.http import HttpResponseRedirect

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework import filters
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from apps.clients.forms import CreateClientForm
from apps.clients.serializers import ClientSerializer, ProvinceSerializer, DistrictSerializer
from apps.clients.utils import valid_client
from apps.common.constants import RUC
from apps.security.views import LoginPermissionView
from .models import Client, Contact, Address, Province, District


class ListClientView(LoginPermissionView, ListView):
    model = Client
    template_name = 'clients/client_list.html'
    context_object_name = 'clients'
    permission_required = 'gesco.clients-list'

    def get_queryset(self):
        if self.request.user.is_superuser:
            return Client.objects.filter(is_deleted=False).order_by('-created_at')

        return self.request.user.clients.filter(is_deleted=False).order_by('-created_at')


class CreateClientView(LoginPermissionView, CreateView):
    model = Client
    form_class = CreateClientForm
    template_name = 'clients/client_create.html'
    success_url = reverse_lazy('clients:client-list')
    permission_required = 'gesco.clients-create'

    def form_valid(self, form, **kwargs):
        contexto = self.get_context_data(**kwargs)
        mensaje = 'Debe ingresar el campo '
        if form.is_valid():
            is_valid, form = valid_client(form, mensaje)
            contexto['form'] = form
        if not form.is_valid():
            contexto['form'] = form
            return self.render_to_response(contexto)
        else:
            client = form.save(commit=False)
            address = form.cleaned_data.get('address')
            phone_1 = form.cleaned_data.get('phone_1')
            phone_2 = form.cleaned_data.get('phone_2')
            email_contact = form.cleaned_data.get('email_contact')
            contact_name = form.cleaned_data.get('contact_name')
            name = form.cleaned_data.get('name')
            paternal_surname = form.cleaned_data.get('paternal_surname')
            maternal_surname = form.cleaned_data.get('maternal_surname')
            first_name = form.cleaned_data.get('first_name')
            second_name = form.cleaned_data.get('second_name')
            district = form.cleaned_data.get('district')

            type_doc = form.cleaned_data.get('type_doc')
            if type_doc != RUC:
                name = '{} {} {} {}'.format(first_name, second_name, paternal_surname, maternal_surname)
            client.name = name
            client.user = self.request.user
            client.save()
            Contact.objects.create(name=contact_name, phone_1=phone_1, phone_2=phone_2, email=email_contact,
                                   client=client)
            Address.objects.create(address=address, client=client, district=district)
        return super(CreateClientView, self).form_valid(form)


class UpdateClientView(LoginPermissionView, UpdateView):
    model = Client
    template_name = 'clients/client_create.html'
    form_class = CreateClientForm
    permission_required = 'gesco.clients-update'
    success_url = reverse_lazy('clients:client-list')

    def form_valid(self, form, **kwargs):
        contexto = self.get_context_data(**kwargs)
        mensaje = 'Debe ingresar el campo '
        if form.is_valid():
            is_valid, form = valid_client(form, mensaje)
            contexto['form'] = form
        if not form.is_valid():
            contexto['form'] = form
            return self.render_to_response(contexto)
        else:
            client = form.save(commit=False)
            address = form.cleaned_data.get('address')
            phone_1 = form.cleaned_data.get('phone_1')
            phone_2 = form.cleaned_data.get('phone_2')
            email_contact = form.cleaned_data.get('email_contact')
            contact_name = form.cleaned_data.get('contact_name')
            name = form.cleaned_data.get('name')
            paternal_surname = form.cleaned_data.get('paternal_surname')
            maternal_surname = form.cleaned_data.get('maternal_surname')
            first_name = form.cleaned_data.get('first_name')
            second_name = form.cleaned_data.get('second_name')
            type_doc = form.cleaned_data.get('type_doc')
            district = form.cleaned_data.get('district')

            if type_doc != RUC:
                name = '{} {} {} {}'.format(first_name, second_name, paternal_surname, maternal_surname)
            client.name = name
            client.save()
            contact = self.object.contacts.last()
            if contact:
                contact.phone_1 = phone_1
                contact.phone_2 = phone_2
                contact.email = email_contact
                contact.name = contact_name
                contact.save(update_fields=['name', 'phone_1', 'phone_2', 'email'])

            object_address = self.object.addresses.last()
            if object_address:
                object_address.address = address
                object_address.district = district
                object_address.save(update_fields=['address', 'district'])
        return super(UpdateClientView, self).form_valid(form)


class DeleteClientView(LoginPermissionView, DeleteView):
    model = Client
    success_url = reverse_lazy('clients:client-list')
    template_name_suffix = '_confirm_delete'
    permission_required = 'gesco.clients-delete'


class RemoveClientView(LoginPermissionView, View):
    permission_required = 'gesco.clients-delete'
    client = None

    def remover(self, request, *args, **kwargs):
        self.client = Client.objects.get(id=self.kwargs['pk'])
        self.client.is_deleted = True
        self.client.save(update_fields={'is_deleted'})
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, *args, **kwargs):
        return self.remover(request, *args, **kwargs)

    def get_success_url(self):
        messages.success(self.request,
                         'Cliente {} eliminado con éxito.'.format(
                             self.client.name))

        return reverse('clients:client-list')


class LoadClientsAPI(ListAPIView):
    serializer_class = ClientSerializer
    queryset = Client.objects.exclude(is_deleted=True)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name',)

    def get_queryset(self):
        c =Client.objects.exclude(is_deleted=True).filter(
            name__icontains=self.request.query_params.get('q'))
        for c in c:
            print(c)
        return Client.objects.exclude(is_deleted=True).filter(
            name__icontains=self.request.query_params.get('q')
        )


class LoadProvincesAPI(ListAPIView):
    serializer_class = ProvinceSerializer

    def get_queryset(self):
        return Province.objects.filter(department=self.kwargs['pk'])


class LoadDistrictsAPI(ListAPIView):
    serializer_class = DistrictSerializer

    def get_queryset(self):
        return District.objects.filter(province=self.kwargs['pk'])


class ValidateRucView(APIView):

    def post(self, request, *args, **kwargs):
        ruc = self.request.POST.get('ruc')
        exception = self.request.POST.get('exception')

        if ruc == exception:
            return Response(status=HTTP_200_OK, data={'alreadyExisted': False})

        already_existed = Client.objects.filter(num_doc__iexact=ruc).filter(type_doc=RUC).count() > 0

        return Response(status=HTTP_200_OK, data={'alreadyExisted': already_existed})
