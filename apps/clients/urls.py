from django.conf.urls import url

from .views import ListClientView, CreateClientView, UpdateClientView, DeleteClientView, RemoveClientView, \
    LoadClientsAPI, LoadProvincesAPI, LoadDistrictsAPI, ValidateRucView

urlpatterns = [
    url(r'^$', ListClientView.as_view(), name='client-list'),
    url(r'nuevo/$', CreateClientView.as_view(), name='client-create'),
    url(r'editar/(?P<pk>\d+)/$', UpdateClientView.as_view(), name='client-edit'),
    url(r'eliminar/(?P<pk>\d+)/$', DeleteClientView.as_view(), name='client-delete'),
    url(r'^cliente/(?P<pk>\d+)/remover/$', RemoveClientView.as_view(),
        name='remover-cliente'),
    url(r'^clientes/$', LoadClientsAPI.as_view(), name='clients-api'),

    url(r'department/(?P<pk>\d+)/provinces$', LoadProvincesAPI.as_view(), name='load-provinces'),
    url(r'province/(?P<pk>\d+)/districts$', LoadDistrictsAPI.as_view(), name='load-districts'),
    url(r'validateRuc/$', ValidateRucView.as_view(), name='validate-ruc'),
]
