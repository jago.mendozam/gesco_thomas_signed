from apps.clients.models import Client
from apps.common.constants import RUC


def valid_client(form, mensaje):
    is_valid = True
    type_doc = form.cleaned_data.get('type_doc')
    num_doc = form.cleaned_data.get('num_doc')
    ruc_exception = form.data.get('rucException')
    first_name = form.cleaned_data.get('first_name')
    paternal_surname = form.cleaned_data.get('paternal_surname')
    email_fact = form.cleaned_data.get('email_fact')
    name = form.cleaned_data.get('name')
    department = form.cleaned_data.get('district')
    district = form.cleaned_data.get('district')

    if type_doc == '6':
        if not name:
            form.errors['name'] = [mensaje + 'Razón Social']
            is_valid = False
    else:
        if not first_name:
            form.errors['first_name'] = [mensaje + 'Primer Nombre']
        if not paternal_surname:
            form.errors['paternal_surname'] = [mensaje + 'Primer Apellido']
        is_valid = False
    if not type_doc:
        form.errors['type_doc'] = [mensaje + 'Tipo Documento']
        is_valid = False
    if not num_doc:
        form.errors['num_doc'] = [mensaje + 'Número de Documento']
        is_valid = False
    else:
        if type_doc == RUC:
            already_existed = False

            if ruc_exception is None:
                # Create View
                already_existed = Client.objects.filter(num_doc__iexact=num_doc).filter(type_doc=RUC).count() > 0
            elif ruc_exception != '' and ruc_exception != num_doc:
                # Edit View
                already_existed = Client.objects.filter(num_doc__iexact=num_doc).filter(type_doc=RUC).count() > 0

            if already_existed:
                form.errors['num_doc'] = ['El RUC ingresado ya está registrado.']
                is_valid = False

    if not email_fact:
        form.errors['email_fact'] = [mensaje + 'Correo de Empresa']
        is_valid = False
    if not department:
        form.errors['department'] = [mensaje + 'Departamento']
        is_valid = False
    if not district:
        form.errors['district'] = [mensaje + 'Distrito']
        is_valid = False

    return is_valid, form
