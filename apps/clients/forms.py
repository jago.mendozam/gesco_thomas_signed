from django import forms
from django.forms import widgets

from apps.clients.models import Client, Department, District
from apps.common.constants import RUC


class CreateClientForm(forms.ModelForm):
    address = forms.CharField(
        label='Dirección del Cliente',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    department = forms.ModelChoiceField(
        label="Departamento",
        queryset=Department.objects.all(),
        required=False,
        widget=forms.Select(attrs={
            "class": "js-select2",
        })
    )
    district = forms.ModelChoiceField(
        label='Distrito',
        queryset=District.objects.all(),
        required=False,
    )
    phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
            'pattern': "[0-9.]+"
        })
    )
    phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control ancho20',
            'pattern': "[0-9.]+"
        })
    )
    email_contact = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    contact_name = forms.CharField(
        label='Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '200',
            'type': 'text',
        })
    )
    type_doc = forms.ChoiceField(
        label="Tipo Documento",
        choices=Client.TYPES_DOCS,
        initial=RUC,
        required=False,
        widget=forms.Select(attrs={
            "class": "js-select2",
        })
    )
    name = forms.CharField(
        required=False,
        label="Razón Social",
        widget=forms.TextInput(
            attrs={
                "maxlength": 120,
                "class": "form-control",
            }
        ))
    paternal_surname = forms.CharField(
        required=False,
        label="Ap. Paterno",
        widget=forms.TextInput(
            attrs={
                "maxlength": 30,
                "class": "form-control",
            }
        ))
    maternal_surname = forms.CharField(
        required=False,
        label="Ap. Materno",
        widget=forms.TextInput(
            attrs={
                "maxlength": 30,
                "class": "form-control",
            }
        ))
    first_name = forms.CharField(
        required=False,
        label="Primer Nombre",
        widget=forms.TextInput(
            attrs={
                "maxlength": 30,
                "class": "form-control",
            }
        ))
    second_name = forms.CharField(
        required=False,
        label="Segundo Nombre",
        widget=forms.TextInput(
            attrs={
                "maxlength": 30,
                "class": "form-control",
            }
        ))
    type_person_sunat = forms.ChoiceField(
        label="Tipo Persona",
        choices=Client.TYPES_PERSON,
        initial=Client.TPJ,
        required=False
    )
    email_fact = forms.EmailField(
        label='Correo de Empresa',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    comments = forms.CharField(
        max_length=1200,
        required=False, label="Descripcion",
        widget=widgets.Textarea(attrs={
            'class': 'form-control no-resize',
            'rows': '2',
            'maxlength': '1200'
        })
    )

    client_group = forms.ChoiceField(
        label="Tipo Cliente:",
        choices=Client.CLIENT_GROUPS,
        required=False
    )

    num_doc = forms.CharField(
        required=False,
        label="Número de Documento",
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
            }
        )
    )
    image = forms.FileField(
        required=False,
        label='Adjuntar',
        widget=widgets.FileInput(attrs={
            'type': 'file',
            'class': 'custom-file-input',
            'accept': 'image/*'
        })
    )

    class Meta:
        model = Client
        fields = ['type_doc', 'num_doc', 'type_person_sunat', 'name', 'paternal_surname', 'maternal_surname',
                  'first_name', 'second_name', 'client_group', 'email_fact', 'comments', 'image', 'address',
                  'contact_name', 'phone_1', 'phone_2', 'email_contact']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        client = self.instance
        address = client.addresses.last()
        contact = client.contacts.last()
        if address:
            self.initial['address'] = address.address
        if contact:
            self.initial['phone_1'] = contact.phone_1
            self.initial['phone_2'] = contact.phone_2
            self.initial['email_contact'] = contact.email
            self.initial['contact_name'] = contact.name

    def clean(self):
        cleaned_data = super().clean()

        district_data = self.data.get('district')

        if district_data is not None and district_data is not '':
            district = District.objects.get(pk=self.data.get('district'))
            cleaned_data['district'] = district

        return cleaned_data
