from django.db import models

from apps.structure.models import Staff


class Client(models.Model):
    OTHERS = 0
    DNI = 1
    FOREIGN_CARD = 4
    RUC = 6
    PASSPORT = 7

    TYPES_DOCS = (
        (OTHERS, 'Otros tipos de documento'),
        (DNI, 'DNI'),
        (FOREIGN_CARD, 'Carnet de extranjería'),
        (RUC, 'RUC'),
        (PASSPORT, 'Pasaporte'),
    )

    TPJ = 'TPJ'
    TPN = 'TPN'
    SND = 'SND'

    TYPES_PERSON = (
        (TPJ, 'Jurídica'),
        (TPN, 'Natural'),
        (SND, 'No domiciliado'),
    )

    STATE = 100
    PRIVATE = 109
    OTHERS_GROUPS = 110

    CLIENT_GROUPS = (
        (STATE, 'Clientes estatales'),
        (PRIVATE, 'Clientes privados'),
        (OTHERS_GROUPS, 'Otros')
    )

    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Fecha Creación:')
    last_modified = models.DateTimeField(auto_now=True, verbose_name='Fecha Modificación:')
    is_deleted = models.BooleanField(default=False, editable=False)
    type_doc = models.PositiveIntegerField(choices=TYPES_DOCS, verbose_name='(*)Identificado con:')
    num_doc = models.CharField(max_length=20, verbose_name='(*)Número de documento:')
    type_person_sunat = models.CharField(max_length=3, choices=TYPES_PERSON, verbose_name='(*)Persona:')
    name = models.CharField(max_length=130, verbose_name='(*)Razón social')
    paternal_surname = models.CharField(max_length=30, null=True, blank=True, verbose_name='(*)Apellido paterno')
    maternal_surname = models.CharField(max_length=30, null=True, blank=True, verbose_name='(*)Apellido materno')
    first_name = models.CharField(max_length=30, null=True, blank=True, verbose_name='(*)Primer nombre')
    second_name = models.CharField(max_length=30, null=True, blank=True, verbose_name='(*)Segundo nombre')
    client_group = models.PositiveIntegerField(verbose_name='(*)Tipo Cliente:', choices=CLIENT_GROUPS)
    locked = models.BooleanField(default=False, verbose_name='Esta bloqueado?')
    email_fact = models.EmailField(blank=True, null=True, verbose_name='(*)Correo de Empresa')
    comments = models.TextField(blank=True, null=True, verbose_name='(*)Comentarios')
    image = models.ImageField(null=True, blank=True, upload_to='clients/images', verbose_name='Imagen')
    full_name = models.CharField(max_length=100, null=True, blank=True, verbose_name="nombre completo")
    user = models.ForeignKey('auth.User', related_name='clients', verbose_name='Usuario', null=True,
                             blank=True)

    class Meta:
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['-created_at', ]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.full_name = self.get_full_name
        super().save(*args, **kwargs)

    @property
    def contact_full_name(self):
        contact = self.contacts.last()
        return contact.full_name if contact else ''

    @property
    def address(self):
        address = self.addresses.last()
        return address.address if address else ''

    @property
    def get_full_name(self):
        nombre_completo = [self.paternal_surname, self.maternal_surname, self.first_name, self.second_name]
        return ' '.join(filter(None, nombre_completo))


class Department(models.Model):
    name = models.CharField(max_length=30)
    ubigeo = models.CharField(max_length=2)

    class Meta:
        verbose_name = 'Departamento'
        verbose_name_plural = 'Departamentos'
        ordering = ['name', ]

    def __str__(self):
        return '{}'.format(self.name)


class Province(models.Model):
    name = models.CharField(max_length=30)
    ubigeo = models.CharField(max_length=4)
    department = models.ForeignKey(Department, related_name='department', verbose_name='Departamento')

    class Meta:
        verbose_name = 'Provincia'
        verbose_name_plural = 'Provincias'
        ordering = ['name', ]

    def __str__(self):
        return '{}'.format(self.name)


class District(models.Model):
    name = models.CharField(max_length=45)
    ubigeo = models.CharField(max_length=6)
    province = models.ForeignKey(Province, related_name='province', verbose_name='Provincia')

    class Meta:
        verbose_name = 'Distrito'
        verbose_name_plural = 'Distritos'
        ordering = ['name', ]

    def __str__(self):
        return '{}'.format(self.name)


class Address(models.Model):
    TAX_RESIDENCE = 'B'

    TYPES_RESIDENCE = (
        (TAX_RESIDENCE, 'Domicilio fiscal'),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False, editable=False)
    type = models.CharField(max_length=1, choices=TYPES_RESIDENCE, default=TAX_RESIDENCE)
    address = models.CharField(max_length=100, verbose_name='Dirección del domicilio fiscal')
    district = models.ForeignKey(District, related_name='district', verbose_name='Distrito', null=True)
    country = models.CharField(max_length=100, verbose_name='País', null=True)
    client = models.ForeignKey(Client, related_name='addresses', verbose_name='Cliente')

    class Meta:
        verbose_name = 'Dirección'
        verbose_name_plural = 'Direcciones'
        ordering = ['-created_at', ]

    def __str__(self):
        return '{}'.format(self.address)


class Contact(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False, editable=False)
    name = models.CharField(max_length=200, verbose_name='Nombre Completo', null=True)
    phone_1 = models.CharField(max_length=20, verbose_name='Teléfono 1', blank=True, null=True)
    phone_2 = models.CharField(max_length=20, verbose_name='Teléfono 2', blank=True, null=True)
    cellphone = models.CharField(max_length=50, blank=True, null=True,
                                 verbose_name='Teléfono móvil')
    email = models.EmailField(verbose_name='Email', blank=True, null=True)
    notes = models.CharField(max_length=200, blank=True, null=True, verbose_name='Notas')
    client = models.ForeignKey(Client, related_name='contacts', verbose_name='Cliente')

    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'
        ordering = ['-created_at', ]

    def __str__(self):
        return '{} {} {}'.format(self.client, self.phone_1, self.email)

    @property
    def full_name(self):
        return '{}'.format(self.name)
