from django.conf.urls import url

from apps.security.views import GescoLoginView, GescoLogoutView, ChangeRoleView, IndexView

urlpatterns = [
    url(r'^login/$', GescoLoginView.as_view(), name='login'),
    url(r'^logout/$', GescoLogoutView.as_view(), name='logout'),
    url(r'^index/$', IndexView.as_view(), name='index'),
    url(r'^cambiar-rol/$', ChangeRoleView.as_view(), name='change-rol'),
]
