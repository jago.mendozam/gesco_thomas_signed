from django.db import models

# Create your models here.
from apps.common.models import BaseModel


class System(BaseModel):
    code = models.CharField(max_length=2, verbose_name='Código')
    name = models.CharField(max_length=30, verbose_name='Nombre')
    image = models.ImageField(upload_to='System/images', null=True, blank=True, verbose_name='Imagen')
    path = models.CharField(max_length=50, null=True, verbose_name='Ruta del Sistema')  # sgd:exp-list

    class Meta:
        verbose_name = 'Sistema'
        verbose_name_plural = 'Sistemas'
        ordering = ('code',)

    def __str__(self):
        return self.name


class Permission(BaseModel):
    name = models.CharField(max_length=50, verbose_name='Nombre')
    code = models.CharField(max_length=40, verbose_name='Código')
    system = models.ForeignKey(System, verbose_name='Sistema',null=True)

    class Meta:
        verbose_name = 'Permiso'
        verbose_name_plural = 'Permisos'

    def __str__(self):
        return self.name


class Role(BaseModel):
    code = models.CharField(max_length=6, unique=True, editable=False, verbose_name='Código')
    name = models.CharField(max_length=40, verbose_name='Nombre')
    system = models.ForeignKey(System, verbose_name='Sistema',null=True)
    permissions = models.ManyToManyField(Permission, related_name='roles', verbose_name='Permisos')

    class Meta:
        verbose_name = 'Rol'
        verbose_name_plural = 'Roles'
        default_related_name = 'roles'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        last_rol = Role.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_rol:
                self.code = str(int(last_rol.code) + 1).zfill(6)
            else:
                self.code = '000001'
            super().save(update_fields=['code'])
