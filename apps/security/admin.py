from django.contrib import admin

# Register your models here.
from apps.security.models import System, Permission, Role
from apps.structure.models import Company


class CompanyAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'name', 'created_at', 'last_modified')
    fields = ('code', 'name')


class SystemAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'last_modified')
    search_fields = ('name', 'code')
    readonly_fields = ('is_deleted',)


class PermissionAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'last_modified')
    search_fields = ('name', 'code')


class RoleAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'last_modified')
    search_fields = ('name', 'code')


admin.site.register(System, SystemAdmin)
admin.site.register(Permission, PermissionAdmin)
admin.site.register(Role, RoleAdmin)
admin.site.register(Company, CompanyAdmin)
