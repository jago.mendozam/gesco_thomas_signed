from django import forms
from django.contrib.auth.forms import AuthenticationForm

from apps.security.models import Role


class AutenticacionForm(AuthenticationForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Usuario',
        })
    )

    password = forms.CharField(
        widget=forms.PasswordInput(attrs={
            'class': 'form-control',
            'placeholder': 'Contraseña',
        })
    )


class ChangeRoleForm(forms.Form):
    role = forms.ModelChoiceField(
        queryset=Role.objects.all()
    )
