from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.http import JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import FormView
from django.views.generic.base import ContextMixin, View, TemplateView, RedirectView

from apps.security.forms import AutenticacionForm, ChangeRoleForm
from apps.security.models import Permission, Role


class GescoLoginView(LoginView):
    template_name = 'authentication/login.html'
    authentication_form = AutenticacionForm

    def form_invalid(self, form):
        messages.error(self.request, 'El usuario y/o contraseña no son válidos.')
        return super(GescoLoginView, self).form_invalid(form)


class GescoLogoutView(LogoutView):
    next_page = reverse_lazy('security:login')


class LoginPermissionView(LoginRequiredMixin, PermissionRequiredMixin, ContextMixin, View):
    login_url = reverse_lazy('security:login')
    permission_exception = False
    roles = []
    permissions = []
    role = None

    def has_permission(self):
        """
        Se cambiara de rol de acuerdo al `permission_required` y se cargara unicamente los roles del sistema de ese
        permiso, se cargara los permisos del primer rol del sistema de acuerdo al `permission_required`
        Estan disponibles las variables en context si es necesario hacer operaciones con ellos
        * usuario_roles
        * rol_actual
        * permisos
        * personal
        """
        code = self.get_permission_required()[0]
        user = self.request.user
        ## problemas con la data indicar como estan tus tablas self.staff trae None
        if hasattr(user, 'staff'):
            self.staff = user.staff
        else:
            messages.warning(self.request, 'El usuario no tiene un Personal asignado en el Sistema.')
            return False

        # Hay vistas que no requiere permisos
        if self.permission_exception:
            return True

        permission = Permission.objects.filter(code=code).first()
        if permission is not None:
            system = permission.system
        else:
            messages.warning(self.request, 'El Permiso {} no existe en el sistema.'.format(code))
            return False

        session = self.request.session
        ## problemas con la data indicar como estan tus tablas self.staff trae None
        self.roles = self.staff.roles.filter(system=system)

        has_permission = False

        if 'role_code' in session:
            self.role = Role.objects.get(code=session['role_code'])

            if self.role.permissions.filter(code=code).count() > 0:
                has_permission = True

        if not has_permission:
            for role in self.roles:
                role_permission = role.permissions.filter(code=code)
                if role_permission.count() > 0:
                    has_permission = True
                    self.role = role
                    break

        if has_permission:
            self.request.session['role'] = self.role.id
            permissions = []

            for permissions_list in self.role.permissions.values('code'):
                permissions_code = permissions_list['code']
                if not (permissions_code in permissions):
                    permissions.append(permissions_code)

            self.permissions = permissions
            self.username = user.username

            return True

        return False

    def get_context_data(self, **kwargs):
        context = super(LoginPermissionView, self).get_context_data(**kwargs)
        context['user_roles'] = self.roles
        context['current_role'] = self.role
        context['permissions'] = self.permissions
        context['staff'] = self.staff
        self.load_role_codes(context)
        return context

    def handle_no_permission(self):
        if self.request.is_ajax():
            return JsonResponse({}, status=403)

        return super(LoginPermissionView, self).handle_no_permission()

    def load_role_codes(self, context):
        # context['codigo_operador_ogd'] = COD_OP_OGD
        # context['codigo_operador_uo'] = COD_OP_UO
        # context['codigo_operador_digitalizador'] = COD_DIGITALIZADOR
        # context['codigo_operador_fiscalizador'] = COD_FISCALIZADOR
        return context


class ChangeRoleView(FormView):
    # Siempre redirigir al index del proyecto cuando se cambia de rol
    success_url = reverse_lazy('sale_opportunity:saleopportunity-list')
    form_class = ChangeRoleForm

    # Redirigir al link anterior si falla el formulario
    def form_invalid(self, form):
        return redirect(self.request.META['HTTP_REFERER'])

    def form_valid(self, form):
        rol = form.cleaned_data.get('role')
        self.request.session['role'] = rol.id
        self.request.session['role_code'] = rol.code
        return super(ChangeRoleView, self).form_valid(form)


class IndexView(LoginPermissionView, TemplateView):
    template_name = 'security/index.html'
    permission_required = 'security.index'
