from django.contrib import admin

# Register your models here.
from apps.products.models import Product, SubProduct


class ProductAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'name', 'created_at', 'last_modified')
    fields = ('code', 'name', )

admin.site.register(Product, ProductAdmin)
admin.site.register(SubProduct)
