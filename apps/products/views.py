from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from rest_framework import filters
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from apps.products.models import Product, SubProduct
from apps.products.seralizers import ProductSerializer


class LoadProductsAPI(ListAPIView):
    serializer_class = ProductSerializer
    queryset = Product.objects.exclude(is_deleted=True)
    filter_backends = (filters.SearchFilter,)
    search_fields = ('name',)

    def get_queryset(self):
        return Product.objects.exclude(is_deleted=True).filter(
            name__icontains=self.request.query_params.get('q')
        )


class LoadSubproductAPIView(APIView):
    def get(self, request):
        try:
            pk = self.request.GET.get('id')
            product = Product.objects.get(pk=pk)
            try:
                subproducts = SubProduct.objects.filter(product=product, is_deleted=False)
            except SubProduct.DoesNotExist:
                subproducts = None
        except Product.DoesNotExist:
            product = None
            return JsonResponse({'status': 400})
        list_subproduct = []
        if subproducts:
            for subproduct in subproducts:
                item_subproducto = {
                    'id': subproduct.id,
                    'text': subproduct.name
                }
                list_subproduct.append(item_subproducto)
        return JsonResponse({'status': 200, 'data': list_subproduct})


class LoadProduct(APIView):
    def get(self, request):
        try:
            send_get = request.GET
            id_subproduct = int(send_get.get('subproduct_id'))
        except(ValueError, TypeError):
            raise ValidationError("Subproducto no valido")
        try:
            subproduct = SubProduct.objects.get(id=id_subproduct)
        except SubProduct.DoesNotExist:
            raise NotFound("Subproducto no encontrado")
        try:
            product = Product.objects.get(subproduct=subproduct)
        except Product.DoesNotExist:
            raise NotFound("Producto no encontrado")
        return Response(status=HTTP_200_OK, data=[product.id])