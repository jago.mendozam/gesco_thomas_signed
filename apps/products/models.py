from django.db import models


# Create your models here.
from apps.common.constants import CORRELATIVO_INICIAL, PREFIJO_PRODUCTO
from apps.common.models import BaseModel


class Product(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Fecha Creación')
    last_modified = models.DateTimeField(auto_now=True, verbose_name='Fecha Modificación')
    is_deleted = models.BooleanField(default=False, editable=False)
    prefix = models.CharField(max_length=3, default=PREFIJO_PRODUCTO)
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    correlative_num = models.PositiveIntegerField(verbose_name='Correlativo', null=True)
    name = models.CharField(verbose_name='Producto', max_length=100, null=True)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
        ordering = ['-created_at', ]
        default_related_name = 'products'

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        last_phase = Product.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_phase:
                aux_correlative = last_phase.correlative_num
                self.correlative_num = aux_correlative + 1
                self.code = '{}-{}'.format(self.prefix, str(self.correlative_num).zfill(2))
            else:
                self.correlative_num = int(CORRELATIVO_INICIAL)
                self.code = '{}-{}'.format(self.prefix, CORRELATIVO_INICIAL.zfill(2))

        super().save(update_fields=['correlative_num', 'code'])


class SubProduct(BaseModel):
    product = models.ForeignKey(Product, verbose_name='Producto')
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    name = models.CharField(verbose_name='SubProducto', max_length=200, null=True)

    class Meta:
        verbose_name = 'SubProduct'
        verbose_name_plural = 'SubProducts'
        default_related_name = 'subproducts'

    def __str__(self):
        return '{}'.format(self.name)
