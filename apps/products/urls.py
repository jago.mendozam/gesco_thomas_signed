from django.conf.urls import url

from apps.products.views import LoadProductsAPI, LoadSubproductAPIView, LoadProduct

urlpatterns = [
    url(r'^productos/$', LoadProductsAPI.as_view(), name='products-api'),
    url(r'^subproductos/api/$', LoadSubproductAPIView.as_view(),
        name='subproducts-api'),
    url(r'^carga-producto/$', LoadProduct.as_view(), name='loadproduct'),
]
