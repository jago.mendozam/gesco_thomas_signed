from django.db import models
from django.utils import timezone


# Create your models here.
class TimeStampedModel(models.Model):
    """ Modelo abstracto TimeStamped """
    created_at = models.DateTimeField(
        blank=True, null=True, editable=False, auto_now_add=True)
    last_modified = models.DateTimeField(
        blank=True, null=True, editable=False, auto_now=True)

    def save(self, *args, **kwargs):
        if self.pk:
            self.created_at = timezone.now()
        else:
            self.last_modified = timezone.now()
            kwargs['force_insert'] = False
        super().save(*args, **kwargs)

    class Meta:
        abstract = True


class StatusModel(models.Model):
    """ Modelo abstracto boleano para el registro """
    is_deleted = models.BooleanField(default=False, verbose_name='Elimina logicamente')

    class Meta:
        abstract = True


class BaseModel(TimeStampedModel, StatusModel):
    """ Modelo Inherente para setear campos  """

    class Meta:
        abstract = True
