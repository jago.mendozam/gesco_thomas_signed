# Inicio de correlativos
CORRELATIVO_INICIAL = "1"

# Prefijo Prioridad
PREFIJO_PRIORIDAD = "PRIO"

# Prefijo para codigo de Fase
PREFIJO_FASE = "FA"

# Prefijo para codigo Producto
PREFIJO_PRODUCTO = 'PRO'

# Default phase
DEFAULT_PHASE = 1

# Types Doc
DNI = "1"
RUC = "6"

CODE_GANADA = 'FA-06'
CODE_NEGOCIACION = 'FA-05'

MESES = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre',
         'Noviembre', 'Diciembre']