import django_filters
from django import forms
from django.contrib.auth.models import User
from django.utils.timezone import now

from apps.clients.models import Client
from apps.products.models import Product, SubProduct
from apps.sale_opportunity.models import AttachedFile, TypeAttachedFile, SaleOpportunity, SaleOpportunityDetail
from apps.sale_opportunity.utils import MyRangeWidget
from apps.structure.models import Staff, Company


class AttachedFileFilter(django_filters.FilterSet):
    type = django_filters.ModelChoiceFilter(
        required=False,
        label='Tipo de Archivo',
        field_name='type',
        queryset=TypeAttachedFile.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
                "placeholder":
                    "Seleccionar Tipo"

            })
    )
    client = django_filters.ModelChoiceFilter(
        label='Cliente',
        field_name='sale_opportunity__client',
        queryset=Client.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
                "placeholder":
                    "Seleccionar Cliente"

            })
    )
    product = django_filters.ModelChoiceFilter(
        label='Producto',
        field_name='sale_opportunity__subproduct__product',
        queryset=Product.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
                "placeholder":
                    "Seleccionar Producto"

            })
    )
    subproduct = django_filters.ModelChoiceFilter(
        label='Sub Producto',
        field_name='sale_opportunity__subproduct',
        queryset=SubProduct.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
                "placeholder":
                    "Seleccionar SubProducto"

            })
    )
    user = django_filters.ModelChoiceFilter(
        label='Administrativo',
        field_name='sale_opportunity__user__staff',
        queryset=Staff.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
                "placeholder":
                    "Seleccionar Administrativo"

            })
    )
    dates = django_filters.DateFromToRangeFilter(
        field_name='created_at',
        widget=MyRangeWidget()
    )
    code_opportunity = django_filters.CharFilter(
        label='Código de Oportunidad de Venta',
        field_name='sale_opportunity__code',
        lookup_expr='icontains',
        widget=forms.TextInput(
            attrs={
                "maxlength": "16",
                "type": "text",
                "class": "form-control",
                "placeholder": "Código de OV"

            })
    )

    class Meta:
        model = AttachedFile
        fields = ['type', 'client', 'product', 'subproduct', 'user', 'dates', 'code_opportunity']


class SaleOpportunityFilter(django_filters.FilterSet):
    product = django_filters.ModelChoiceFilter(
        label='Producto',
        field_name='subproduct__product',
        queryset=Product.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:95%",
                "class": "form-control js-select2",
                "placeholder":
                    "Seleccionar Producto"
            })
    )
    subproduct = django_filters.ModelChoiceFilter(
        label='Sub Producto',
        field_name='subproduct',
        queryset=SubProduct.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:95%",
                "class": "form-control js-select2",
                "placeholder":
                    "Seleccionar SubProducto"

            })
    )
    client = django_filters.ModelChoiceFilter(
        label='Cliente',
        field_name='client',
        queryset=Client.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:95%",
                "class": "form-control js-select2",
                "placeholder":
                    "Seleccionar Cliente"

            })
    )
    user = django_filters.ModelChoiceFilter(
        label='Ejecutivo',
        field_name='user__staff',
        queryset=Staff.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:95%",
                "class": "form-control js-select2",
                "placeholder":
                    "Seleccionar Administrativo"
            })
    )
    dates = django_filters.DateFromToRangeFilter(
        field_name='created_at',
        widget=MyRangeWidget(attrs={
            "style": "width:95%",
            "class": "form-control js-select2",
            "placeholder":
                "Seleccionar rango de fechas de registro"
        })
    )
    company = django_filters.ModelChoiceFilter(
        label='Empresa',
        field_name='company__name',
        queryset=Company.objects.filter(is_deleted=False),
        widget=forms.Select(
            attrs={
                "style": "width:95%",
                "class": "form-control js-select2",
                "placeholder":
                    "Seleccionar Empresa"
            })
    )

    def __init__(self,  *args, **kwargs):
        super(SaleOpportunityFilter, self).__init__(*args, **kwargs)

        if not self.request.user.is_superuser:
            self.filters['user'].queryset = Staff.objects.filter(user=self.request.user)

    class Meta:
        model = SaleOpportunity
        fields = ['product', 'subproduct', 'client', 'user', 'dates', 'company']


class AnioFilter(django_filters.FilterSet):
    sale_anio = django_filters.ChoiceFilter(
        label="Año",
        required=True,
        initial=now().year,
        choices=tuple((now().year - i, now().year - i) for i in range(10)),
        widget=forms.Select(attrs={
            "maxlength": "4", "type": "text",
            "class": "form-control js-select2",
            "placeholder": "Año"}))

    class Meta:
        model = SaleOpportunity
        fields = ['sale_anio']


class AnioVariationNewFilter(django_filters.FilterSet):
    anio = django_filters.ChoiceFilter(
        required=True,
        initial=now().year +2,
        choices=tuple(((now().year - i) + 2, (now().year - i) + 2) for i in range(10)),
        field_name='month_year',
        lookup_expr='icontains',
        widget=forms.Select(attrs={
            "maxlength": "4", "type": "text",
            "class": "form-control js-select2",
            "placeholder": "Año"}))

    class Meta:
        model = SaleOpportunityDetail
        fields = ['anio']