from django import template

register = template.Library()


@register.filter(name='currency')
def currency(field):
    return "S/ {:,}".format(round(field, 2))
