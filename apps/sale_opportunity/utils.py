from datetime import date

import django_filters
from django import forms
from django_filters.widgets import RangeWidget

from apps.sale_opportunity.forms import AttachedFileForm


class MyRangeWidget(forms.MultiWidget):
    def __init__(self, attrs=None):
        widgets = (
            forms.TextInput(
                attrs={
                    'id': 'fecha_ini',
                    'type': 'date',
                    'placeholder': 'fecha',
                    'class': 'form_control ',
                    'style': 'margin-bottom: 20px; '
                             'display: inline-block; '
                             'width: 40%; '
                             'margin-right: 30px'
                }),
            forms.TextInput(
                attrs={
                    'type': 'date',
                    'class': 'form_control ',
                    'style': 'display: inline; width: 40%'
                }))
        super(MyRangeWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.start, value.stop]
        return [None, None]


class MyRangeDateWidget(forms.MultiWidget):
    def __init__(self, attrs=None):
        hoy = date.today()
        widgets = (
            forms.TextInput(
                attrs={'type': 'date',
                       'max': hoy,
                       'class': 'form-control',
                       'style': 'width:48%;display:inline'}),
            forms.TextInput(
                attrs={'type': 'date',
                       'max': hoy,
                       'class': 'form-control',
                       'style': 'width:48%;display:inline;'
                                'float: right;'}))
        super(MyRangeDateWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            return [value.start, value.stop]
        return [None, None]


class StringRangeField(forms.MultiValueField):
    widget = RangeWidget

    def __init__(self, fields=None, *args, **kwargs):
        if fields is None:
            fields = (
                forms.CharField(),
                forms.CharField())
        super(StringRangeField, self).__init__(fields, *args, **kwargs)

    def compress(self, data_list):
        if data_list:
            return slice(*data_list)
        return None


class StringRangeFilter(django_filters.RangeFilter):
    field_class = StringRangeField


def valid_adjuntos(formset):
    valid = True
    i = 0
    for attached_file in formset:
        # Saving in the contacts models
        if formset[0].cleaned_data:
            file_doc = attached_file.cleaned_data.get('file_doc')
            type = attached_file.cleaned_data.get('type')
            description = attached_file.cleaned_data.get('description')
            if not file_doc:
                formset[i].errors['file_doc'] = attached_file.fields.get('file_doc').error_messages.get('missing')
                valid = False
            if not type:
                formset[i].errors['type'] = attached_file.fields.get('type').error_messages.get('required')
                valid = False
            if not description:
                formset[i].errors['description'] = attached_file.fields.get('description').error_messages.get('required')
                valid = False
            i += 1
    return valid, formset


def valid_opportunity(form, mensaje):
    is_valid = True
    phase = form.cleaned_data.get('phase')
    revenue = form.cleaned_data.get('revenue')
    real_revenue = form.cleaned_data.get('real_revenue')
    quantity = form.cleaned_data.get('quantity')
    unit_length = form.cleaned_data.get('unit_length')
    reason_loss = form.cleaned_data.get('reason_loss')
    client = form.cleaned_data.get('client')
    product = form.cleaned_data.get('product')
    subproduct = form.cleaned_data.get('subproduct')
    priority = form.cleaned_data.get('priority')
    company = form.cleaned_data.get('company')
    contact_way = form.cleaned_data.get('contact_way')
    installments = form.cleaned_data.get('installments')
    installment_period = form.cleaned_data.get('installment_period')
    profit = form.cleaned_data.get('profit')
    contact = form.cleaned_data.get('contact')
    contact_phone_1 = form.cleaned_data.get('contact_phone_1')
    contact_email = form.cleaned_data.get('contact_email')
    manual_code = form.cleaned_data.get('manual_code')
    first_payment_month = form.cleaned_data.get('first_payment_month')
    first_payment_year = form.cleaned_data.get('first_payment_year')

    if not client:
        form.errors['client'] = [mensaje + 'Cliente']
        is_valid = False
    if not product:
        form.errors['product'] = [mensaje + 'Producto']
        is_valid = False
    if not subproduct:
        form.errors['subproduct'] = [mensaje + 'Subproducto']
        is_valid = False
    if not priority:
        form.errors['priority'] = [mensaje + 'Prioridad']
        is_valid = False
    if not company:
        form.errors['company'] = [mensaje + 'Empresa']
        is_valid = False
    if not contact_way:
        form.errors['contact_way'] = [mensaje + 'Medio de Contacto']
        is_valid = False
    if not revenue:
        form.errors['revenue'] = [mensaje + 'Ingreso Estimado']
        is_valid = False

    #if not profit:
    #    if profit != 0:
    #        form.errors['profit'] = [mensaje + 'Ganacia']
    #        is_valid = False
    if not contact:
        form.errors['contact'] = [mensaje + 'Contacto']
        is_valid = False
    if not contact_phone_1:
        form.errors['contact_phone_1'] = [mensaje + 'Telf 1']
        is_valid = False
    if not contact_email:
        form.errors['contact_email'] = [mensaje + 'Email Contacto']
        is_valid = False
    # Negociación o Ganada
    if phase.code == 'FA-05' or phase.code == 'FA-06':
        if not installments:
            form.errors['installments'] = [mensaje + 'Cuotas']
            is_valid = False
        if not installment_period:
            form.errors['installment_period'] = [mensaje + 'Intervalo']
            is_valid = False
        if not real_revenue:
            form.errors['real_revenue'] = [mensaje + 'Ingreso Real']
            is_valid = False
        if not quantity:
            form.errors['quantity'] = [mensaje + 'Cantidad']
            is_valid = False
        if not unit_length:
            form.errors['unit_length'] = [mensaje + 'Unidad de medida']
            is_valid = False
        # Solo en ganadas
        if phase.code == 'FA-06':
            if not first_payment_month:
                form.errors['first_payment_month'] = [mensaje + 'Mes de facturación']
                is_valid = False
            if int(first_payment_year) == 0:
                form.errors['first_payment_year'] = [mensaje + 'Año de facturación']
                is_valid = False
    elif phase.code == 'FA-07':
        if not reason_loss:
            form.errors['reason_loss'] = [mensaje + 'Razón de Pérdida']
            is_valid = False
    if not manual_code:
        form.errors['manual_code'] = [mensaje + 'Código de Oportunidad']
        is_valid = False
    return is_valid, form


def valid_create_activity(form, mensaje, is_edit):
    is_valid = True
    description = form.cleaned_data.get('description')
    start_date = form.cleaned_data.get('start_date')
    end_date = form.cleaned_data.get('end_date')
    type = form.cleaned_data.get('type')
    result = form.cleaned_data.get('result')

    if not description:
        form.errors['description'] = [mensaje + 'Descripción']
        is_valid = False
    if not start_date:
        form.errors['start_date'] = [mensaje + 'Fecha de Inicio']
        is_valid = False
    if not end_date:
        form.errors['end_date'] = [mensaje + 'Fecha de Vencimiento']
        is_valid = False
    if not type:
        form.errors['type'] = [mensaje + 'Tipo de Archivo']
        is_valid = False
    if is_edit:
        if not result:
            form.errors['result'] = [mensaje + 'Estado']
            is_valid = False
    return is_valid, form


def stringMonth(key):
    if key == 1:
        month = 'ENERO'
    elif key == 2:
        month = 'FEBRERO'
    elif key == 3:
        month = 'MARZO'
    elif key == 4:
        month = 'ABRIL'
    elif key == 5:
        month = 'MAYO'
    elif key == 6:
        month = 'JUNIO'
    elif key == 7:
        month = 'JULIO'
    elif key == 8:
        month = 'AGOSTO'
    elif key == 9:
        month = 'SETIEMBRE'
    elif key == 10:
        month = 'OCTUBRE'
    elif key == 11:
        month = 'NOVIEMBRE'
    elif key == 12:
        month = 'DICIEMBRE'
    return month
