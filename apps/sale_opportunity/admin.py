from django.contrib import admin

# Register your models here.
from apps.sale_opportunity.models import SaleOpportunity, Phase, SaleOpportunityDetail, AttachedFile, TypeAttachedFile, \
    Activity, Result, Priority, ContactWay, ReasonLoss, TypeActivity, UnitLength


class ActivityInline(admin.TabularInline):
    model = Activity
    extra = 0
    verbose_name = 'Actividad'
    verbose_name_plural = 'Actividades'
    can_delete = False


class SaleOpportunityDetailInline(admin.TabularInline):
    model = SaleOpportunityDetail
    extra = 0
    verbose_name = 'Detalle'
    verbose_name_plural = 'Detalles'
    can_delete = False


class SaleOpportunityAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('manual_code', 'code', 'client', 'phase', 'subproduct', 'revenue', 'real_revenue', 'estimated_time', 'created_at', 'last_modified', 'sale_anio')
    fields = ('manual_code', 'code', 'client', 'phase', 'subproduct', 'revenue', 'real_revenue', 'estimated_time', 'user', 'sale_anio')
    inlines = [SaleOpportunityDetailInline, ]


class PriorityAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'name', 'created_at', 'last_modified')
    fields = ('code', 'name')


class ContactWayAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'name', 'created_at', 'last_modified')
    fields = ('code', 'name')


class ReasonLossAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'name', 'created_at', 'last_modified')
    fields = ('code', 'name')


class PhaseAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'name', 'probability', 'created_at', 'last_modified')
    fields = ('code', 'name', 'probability')


class AttachedFileAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'file_doc', 'description', 'type', 'sale_opportunity')


class ResultAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'name', 'created_at', 'last_modified')
    fields = ('code', 'name')


class TypeActivityAdmin(admin.ModelAdmin):
    readonly_fields = ('code',)
    list_display = ('code', 'name', 'created_at', 'last_modified')
    fields = ('code', 'name')


admin.site.register(SaleOpportunity, SaleOpportunityAdmin)
admin.site.register(Phase, PhaseAdmin)
admin.site.register(SaleOpportunityDetail)
admin.site.register(AttachedFile, AttachedFileAdmin)
admin.site.register(TypeAttachedFile)
admin.site.register(Result, ResultAdmin)
admin.site.register(Priority, PriorityAdmin)
admin.site.register(ContactWay, ContactWayAdmin)
admin.site.register(ReasonLoss, ReasonLossAdmin)
admin.site.register(TypeActivity, TypeActivityAdmin)
admin.site.register(UnitLength)
