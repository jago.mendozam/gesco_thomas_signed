from datetime import date

from django import forms
from django.forms import widgets

from apps.clients.models import Client
from apps.common.constants import DEFAULT_PHASE
from apps.products.models import Product, SubProduct
from .models import SaleOpportunity, AttachedFile, TypeAttachedFile, Phase, Activity, Priority, ContactWay, \
    ReasonLoss, Result, TypeActivity, UnitLength

from apps.structure.models import Company

CURRENT_YEAR = date.today().year


class FormCreateSaleOpportunity(forms.ModelForm):
    client = forms.ModelChoiceField(
        label="Cliente",
        queryset=Client.objects.filter(is_deleted=False),
        required=False
    )
    installments = forms.IntegerField(
        label="Cuotas",
        min_value=1,
        max_value=99,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '2',
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )
    installment_period = forms.IntegerField(
        label="Intervalo",
        min_value=1,
        max_value=12,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '2',
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )

    phase = forms.ModelChoiceField(
        label="Fase",
        queryset=Phase.objects.filter(is_deleted=False),
        initial=DEFAULT_PHASE,
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Doc.Iden.'
        })
    )

    product = forms.ModelChoiceField(
        label="Producto",
        queryset=Product.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
            })
    )
    subproduct = forms.ModelChoiceField(
        label="Subproducto",
        queryset=SubProduct.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
            })
    )
    revenue = forms.IntegerField(
        min_value=1,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        label='Ingreso Estimado (S/.)',
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '10',
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )
    profit = forms.FloatField(
        min_value=0,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        label='Ganancia',
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '10',
            'type': 'number',
            'class': 'form-control',
            'min': '0',
        })
    )
    description = forms.CharField(
        max_length=1200,
        required=False,
        label="Descripción de la oportunidad de venta",
        widget=widgets.Textarea(attrs={
            'class': 'form-control no-resize',
            'rows': '2',
            'maxlength': '1200'
        })
    )
    priority = forms.ModelChoiceField(
        label="Prioridad",
        queryset=Priority.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Prioridad'
        })
    )
    contact_way = forms.ModelChoiceField(
        label="Medio de Contacto",
        queryset=ContactWay.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Medio de Contacto'
        })
    )
    company = forms.ModelChoiceField(
        label="Empresa",
        queryset=Company.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Empresa'
        })
    )
    contact = forms.CharField(
        label='Nombre',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    contact_position = forms.CharField(
        label='Cargo',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '60',
            'type': 'text',
        })
    )
    contact_phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_email = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    # Contacto 2
    contact_2 = forms.CharField(
        label='Nombre',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    contact_position_2 = forms.CharField(
        label='Cargo',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '60',
            'type': 'text',
        })
    )
    contact_2_phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_2_phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_email_2 = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    # Contacto 3
    contact_3 = forms.CharField(
        label='Nombre',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    contact_position_3 = forms.CharField(
        label='Cargo',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '60',
            'type': 'text',
        })
    )
    contact_3_phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_3_phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_email_3 = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    # Contacto 4
    contact_4 = forms.CharField(
        label='Nombre',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    contact_position_4 = forms.CharField(
        label='Cargo',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '60',
            'type': 'text',
        })
    )
    contact_4_phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_4_phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_email_4 = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    reason_loss = forms.ModelChoiceField(
        label="Razón de Perdida",
        queryset=ReasonLoss.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Razón de Perdida'
        })
    )
    manual_code = forms.CharField(
        label='Código',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '16',
            'type': 'text',
        })
    )

    class Meta:
        model = SaleOpportunity
        fields = ['client', 'profit', 'product', 'subproduct', 'phase', 'revenue', 'installments', 'description',
                  'priority', 'contact_way', 'company', 'contact', 'contact_position', 'contact_phone_1',
                  'contact_phone_2', 'contact_email', 'contact_2', 'contact_position_2', 'contact_2_phone_1',
                  'contact_2_phone_2', 'contact_email_2', 'contact_3', 'contact_position_3', 'contact_3_phone_1',
                  'contact_3_phone_2', 'contact_email_3', 'contact_4', 'contact_position_4', 'contact_4_phone_1',
                  'contact_4_phone_2', 'contact_email_4', 'reason_loss', 'manual_code', 'installment_period']


class AttachedFileForm(forms.ModelForm):
    id = forms.CharField(required=False, widget=widgets.FileInput(attrs={
        'type': 'text'
    }))
    file_doc = forms.FileField(required=False, label='Adjuntar',
                               widget=widgets.FileInput(attrs={
                                   'type': 'file',
                                   'class': 'custom-file-input',
                                   'accept': '.jpg, .jpeg, .png'
                               })
                               )
    description = forms.CharField(
        required=False, label="Descripción de la oportunidad de venta",
        widget=widgets.Textarea(attrs={
            'class': 'form-control no-resize',
            'rows': '2',
            'maxlength': '200'
        })
    )
    type = forms.ModelChoiceField(
        queryset=TypeAttachedFile.objects.all(),
        required=False,
        label="Tipo de documento",
        widget=forms.Select(attrs={
            'class': 'ancho100',
            'placeholder': 'Tipo archivo'
        })
    )
    # path_for_file_doc = forms.FileField(required=False, widget=forms.HiddenInput)
    path_file_doc = forms.CharField(required=False, widget=widgets.HiddenInput)

    class Meta:
        model = AttachedFile
        fields = ['id', 'file_doc', 'description', 'type', 'path_file_doc']


class FormEditSaleOpportunity(forms.ModelForm):
    client = forms.ModelChoiceField(
        label="Cliente",
        queryset=Client.objects.filter(is_deleted=False),
        required=False
    )
    installments = forms.IntegerField(
        min_value=1,
        max_value=99,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        label='Cuotas',
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '2',
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )
    installment_period = forms.IntegerField(
        label='Intervalo',
        min_value=1,
        max_value=12,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '2',
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )
    phase = forms.ModelChoiceField(
        label="Fase",
        queryset=Phase.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Doc.Iden.'
        })
    )
    product = forms.ModelChoiceField(
        label="Producto",
        queryset=Product.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
            })
    )
    subproduct = forms.ModelChoiceField(
        label="Subproducto",
        queryset=SubProduct.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
            })
    )
    revenue = forms.IntegerField(
        min_value=1,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        label='Ingreso Estimado (S/.)',
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '10',
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )
    real_revenue = forms.IntegerField(
        min_value=1,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        label='Ingreso Real (S/.)',
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '10',
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )
    quantity = forms.IntegerField(
        min_value=1,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        label='Cantidad',
        required=False,
        widget=forms.NumberInput(attrs={
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )
    unit_length = forms.ModelChoiceField(
        label="Unidad de medida",
        queryset=UnitLength.objects.filter(is_deleted=False),
        required=False,
        empty_label='',
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "js-select2",
            })
    )
    profit = forms.FloatField(
        min_value=0,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        label='Ganancia',
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '10',
            'type': 'number',
            'class': 'form-control',
            'min': '0',
        })
    )
    description = forms.CharField(
        max_length=1200,
        required=False,
        label="Descripción",
        widget=widgets.Textarea(attrs={
            'class': 'form-control no-resize',
            'rows': '2',
            'maxlength': '1200'
        })
    )
    priority = forms.ModelChoiceField(
        label="Prioridad",
        queryset=Priority.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Prioridad'
        })
    )
    contact_way = forms.ModelChoiceField(
        label="Medio de Contacto",
        queryset=ContactWay.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Medio de Contacto'
        })
    )
    company = forms.ModelChoiceField(
        label="Empresa",
        queryset=Company.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Empresa'
        })
    )
    contact = forms.CharField(
        label='Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    contact_position = forms.CharField(
        label='Cargo',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '20',
            'type': 'text',
        })
    )
    contact_phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
            'pattern': "[0-9.]+"
        })
    )
    contact_phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
            'pattern': "[0-9.]+"
        })
    )
    contact_email = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    # Contacto 2
    contact_2 = forms.CharField(
        label='Nombre',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    contact_position_2 = forms.CharField(
        label='Cargo',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '60',
            'type': 'text',
        })
    )
    contact_2_phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_2_phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_email_2 = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    # Contacto 3
    contact_3 = forms.CharField(
        label='Nombre',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    contact_position_3 = forms.CharField(
        label='Cargo',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '60',
            'type': 'text',
        })
    )
    contact_3_phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_3_phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_email_3 = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    # Contacto 4
    contact_4 = forms.CharField(
        label='Nombre',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '100',
            'type': 'text',
        })
    )
    contact_position_4 = forms.CharField(
        label='Cargo',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '60',
            'type': 'text',
        })
    )
    contact_4_phone_1 = forms.CharField(
        label='Teléfono 1',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_4_phone_2 = forms.CharField(
        label='Teléfono 2',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '9',
            'type': 'text',
            'class': 'form-control',
        })
    )
    contact_email_4 = forms.EmailField(
        label='Email Contacto',
        required=False,
        widget=forms.TextInput(attrs={
            'class': 'form-control',
            'type': 'email',
        })
    )
    reason_loss = forms.ModelChoiceField(
        label="Razón de Perdida",
        queryset=ReasonLoss.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(attrs={
            'placeholder': 'Razón de Perdida'
        })
    )
    manual_code = forms.CharField(
        label='Código',
        required=False,
        widget=forms.TextInput(attrs={
            'maxlength': '16',
            'type': 'text',
        })
    )
    first_payment_month = forms.IntegerField(
        label='Mes (Facturación)',
        min_value=1,
        max_value=12,
        error_messages={
            'min_value': 'Debe ser mayor o igual a 1',
        },
        required=False,
        widget=forms.NumberInput(attrs={
            'maxlength': '2',
            'type': 'number',
            'class': 'form-control',
            'min': '1',
            'step': '1',
        })
    )
    first_payment_year = forms.ChoiceField(
        label='Año (Facturación)',
        choices=(
            (0, ''),
            (CURRENT_YEAR, CURRENT_YEAR),
            (CURRENT_YEAR + 1, CURRENT_YEAR + 1),
        ),
        required=False,
    )

    class Meta:
        model = SaleOpportunity
        fields = ['client', 'profit', 'product', 'subproduct', 'phase', 'revenue', 'real_revenue', 'installments',
                  'description', 'priority', 'contact_way', 'company', 'contact', 'contact_position', 'contact_phone_1',
                  'contact_phone_2', 'contact_email', 'contact_2', 'contact_position_2', 'contact_2_phone_1',
                  'contact_2_phone_2', 'contact_email_2', 'contact_3', 'contact_position_3', 'contact_3_phone_1',
                  'contact_3_phone_2', 'contact_email_3', 'contact_4', 'contact_position_4', 'contact_4_phone_1',
                  'contact_4_phone_2', 'contact_email_4', 'reason_loss', 'manual_code', 'installment_period',
                  'first_payment_month', 'first_payment_year', 'quantity', 'unit_length']


class CreateActivityForm(forms.ModelForm):
    description = forms.CharField(
        label='Descripción',
        required=True,
        max_length=200,
        widget=forms.Textarea(
            attrs={'rows': 2,
                   'style': 'resize:none;',
                   'class': 'form-control'}
        )
    )
    start_date = forms.DateField(
        label='Fecha de Inicio',
        required=True,
        widget=forms.DateInput(
            attrs={
                'class': 'form-control',
                'type': 'date'
            }
        )
    )
    end_date = forms.DateField(
        label='Fecha de Vencimiento',
        required=True,
        widget=forms.DateInput(
            attrs={
                'class': 'form-control',
                'type': 'date'
            }
        )
    )
    type = forms.ModelChoiceField(
        label='Tipo Actividad',
        queryset=TypeActivity.objects.filter(is_deleted=False),
        required=True,
        widget=forms.Select(attrs={
            "style": "width:100%",
            "class": "form-control js-select2",
        })
    )
    result = forms.ModelChoiceField(
        label="Resultado",
        queryset=Result.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "form-control js-select2",
            })
    )

    class Meta:
        model = Activity
        fields = ['description', 'start_date', 'end_date', 'type']


class UpdateActivityForm(forms.ModelForm):
    description = forms.CharField(
        label='Descripción',
        required=False,
        max_length=200,
        widget=forms.Textarea(
            attrs={'rows': 2,
                   'style': 'resize:none;',
                   'class': 'form-control'}
        )
    )
    start_date = forms.DateField(
        label='Fecha de Inicio',
        required=False,
        widget=forms.DateInput(
            format='%Y-%m-%d',
            attrs={
                'class': 'form-control',
                'type': 'date'
            }
        )
    )
    end_date = forms.DateField(
        label='Fecha de Vencimiento',
        required=False,
        widget=forms.DateInput(
            format='%Y-%m-%d',
            attrs={
                'class': 'form-control',
                'type': 'date'
            }
        )
    )
    type = forms.ModelChoiceField(
        label='Tipo Actividad',
        queryset=TypeActivity.objects.filter(is_deleted=False),
        required=True,
        widget=forms.Select(attrs={
            "style": "width:100%",
            "class": "form-control js-select2",
        })
    )
    result = forms.ModelChoiceField(
        label="Resultado",
        queryset=Result.objects.filter(is_deleted=False),
        required=False,
        widget=forms.Select(
            attrs={
                "style": "width:100%",
                "class": "form-control js-select2",
            })
    )
    meeting_result = forms.CharField(
        label='Resultado de Actividad',
        required=False,
        max_length=2000,
        widget=forms.Textarea(
            attrs={
                'rows': 4,
                'style': 'resize:none;',
                'class': 'form-control'
            }
        )
    )

    class Meta:
        model = Activity
        fields = ['description', 'start_date', 'end_date', 'type', 'result', 'meeting_result']
