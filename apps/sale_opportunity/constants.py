ENERO = '01'
FEBRERO = '02'
MARZO = '03'
ABRIL = '04'
MAYO = '05'
JUNIO = '06'
JULIO = '07'
AGOSTO = '08'
SETIEMBRE = '09'
OCTUBRE = '10'
NOVIEMBRE = '11'
DICIEMBRE = '12'

MONTHS = (
            (ENERO, 'Enero'),
            (FEBRERO, 'Febrero'),
            (MARZO, 'Marzo'),
            (ABRIL, 'Abril'),
            (MAYO, 'Mayo'),
            (JUNIO, 'Junio'),
            (JULIO, 'Julio'),
            (AGOSTO, 'Agosto'),
            (SETIEMBRE, 'Setiembre'),
            (OCTUBRE, 'Octubre'),
            (NOVIEMBRE, 'Noviembre'),
            (DICIEMBRE, 'Diciembre')
        )

COD_FILE_TERMINOS = '000001'
COD_FILE_CONTRATO = '000002'
COD_FILE_CONSULTA = '000003'
COD_FILE_BASES = '000004'
COD_FILE_BUENA = '000005'
COD_FILE_COTIZACION = '000006'