import os
from datetime import date
from django.core.validators import MaxValueValidator
from django.db import models

# Create your models here.
from django.utils import timezone

from apps.clients.models import Client
from apps.common.constants import CORRELATIVO_INICIAL, PREFIJO_FASE, PREFIJO_PRIORIDAD
from apps.common.models import BaseModel
from apps.products.models import Product, SubProduct
from apps.structure.models import Company


def upload_file(instance, filename):
    ext = filename.split('.')[-1]
    filename = "opventa_doc_{0}.{1}".format(timezone.now(), ext)
    return "%s/%s" % (instance._meta.app_label, filename)


class Priority(BaseModel):
    correlative = models.PositiveIntegerField(verbose_name='Correlativo', null=True)
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    name = models.CharField(verbose_name='Nombre', max_length=100)

    class Meta:
        verbose_name = 'Priority'
        verbose_name_plural = 'Priorities'
        default_related_name = 'priorities'

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        last_priority = Priority.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_priority:
                aux_correlative = last_priority.correlative
                self.correlative = aux_correlative + 1
                self.code = '{}-{}'.format(PREFIJO_PRIORIDAD, str(self.correlative).zfill(2))
            else:
                self.correlative = int(CORRELATIVO_INICIAL)
                self.code = '{}-{}'.format(PREFIJO_PRIORIDAD, CORRELATIVO_INICIAL.zfill(2))
        super().save(update_fields=['correlative', 'code'])


class ContactWay(BaseModel):
    correlative = models.PositiveIntegerField(verbose_name='Correlativo', null=True)
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    name = models.CharField(verbose_name='Nombre', max_length=100)

    class Meta:
        verbose_name = 'ContactWay'
        verbose_name_plural = 'ContactWays'
        default_related_name = 'contactWays'

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        last_contact_way = ContactWay.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_contact_way:
                aux_correlative = last_contact_way.correlative
                self.correlative = aux_correlative + 1
                self.code = str(self.correlative).zfill(2)
            else:
                self.correlative = int(CORRELATIVO_INICIAL)
                self.code = CORRELATIVO_INICIAL.zfill(2)
        super().save(update_fields=['correlative', 'code'])


class ReasonLoss(BaseModel):
    correlative = models.PositiveIntegerField(verbose_name='Correlativo', null=True)
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    name = models.CharField(verbose_name='Nombre', max_length=100)

    class Meta:
        verbose_name = 'ReasonLoss'
        verbose_name_plural = 'ReasonsLoss'
        default_related_name = 'reasonsLoss'

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        last_reason = ReasonLoss.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_reason:
                aux_correlative = last_reason.correlative
                self.correlative = aux_correlative + 1
                self.code = str(self.correlative).zfill(2)
            else:
                self.correlative = int(CORRELATIVO_INICIAL)
                self.code = CORRELATIVO_INICIAL.zfill(2)
        super().save(update_fields=['correlative', 'code'])


class Phase(BaseModel):
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    correlative_num = models.PositiveIntegerField(verbose_name='Correlativo', null=True)
    name = models.CharField(verbose_name='Fase', max_length=100)
    probability = models.PositiveIntegerField(verbose_name='Probabilidad', validators=[MaxValueValidator(100)])
    is_first = models.BooleanField(default=False)
    is_last = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Phase'
        verbose_name_plural = 'Phases'
        default_related_name = 'phases'

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        last_phase = Phase.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_phase:
                aux_correlative = last_phase.correlative_num
                self.correlative_num = aux_correlative + 1
                self.code = '{}-{}'.format(PREFIJO_FASE, str(self.correlative_num).zfill(2))
            else:
                self.correlative_num = int(CORRELATIVO_INICIAL)
                self.code = '{}-{}'.format(PREFIJO_FASE, CORRELATIVO_INICIAL.zfill(2))

        super().save(update_fields=['correlative_num', 'code'])


class UnitLength(BaseModel):
    name = models.CharField(verbose_name='Nombre', max_length=20)

    class Meta:
        verbose_name = 'Unidad de medida'
        verbose_name_plural = 'Unidades de medida'

    def __str__(self):
        return self.name


class SaleOpportunity(BaseModel):
    code = models.CharField(verbose_name='Código', max_length=16, null=True)
    sale_opp_num = models.PositiveIntegerField(verbose_name='Correlativo de Oportunidad', null=True)
    sale_anio = models.CharField(max_length=4,
                                 default=timezone.now().strftime('%Y'))
    client = models.ForeignKey(Client, related_name='clientes', verbose_name='Cliente')
    phase = models.ForeignKey(Phase, on_delete=models.CASCADE, null=True, verbose_name='Fase de Venta')
    subproduct = models.ForeignKey(SubProduct, on_delete=models.CASCADE, null=True, verbose_name='Subproducto')
    description = models.TextField(blank=True, null=True, verbose_name='Descripcion')
    revenue = models.PositiveIntegerField(verbose_name='Ingreso Estimado', null=True)
    real_revenue = models.PositiveIntegerField(verbose_name='Ingreso Real', null=True)
    estimated_time = models.PositiveIntegerField(verbose_name='Tiempo Estimado', validators=[MaxValueValidator(99)],
                                                 null=True)
    profit = models.FloatField(null=True, blank=True, verbose_name="Ganancia")
    priority = models.ForeignKey(Priority, on_delete=models.CASCADE, verbose_name='Prioridad', null=True, blank=True)
    contact_way = models.ForeignKey(ContactWay, on_delete=models.CASCADE, verbose_name='Medio de Contacto',
                                    null=True, blank=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, verbose_name='Empresa', null=True, blank=True)
    user = models.ForeignKey('auth.User', related_name='sale_opportunities', verbose_name='User', null=True)

    contact = models.CharField(max_length=100, verbose_name='Contacto', null=True, blank=True)
    contact_position = models.CharField(max_length=60, null=True, blank=True, verbose_name='Cargo de Contacto')
    contact_phone_1 = models.CharField(max_length=15, verbose_name='Teléfono 1 de Contacto', blank=True, null=True)
    contact_phone_2 = models.CharField(max_length=15, verbose_name='Teléfono 2 de Contacto', blank=True, null=True)
    contact_email = models.EmailField(verbose_name='Email de Contacto', blank=True, null=True)
    contact_2 = models.CharField(max_length=100, verbose_name='Contacto 2', null=True, blank=True)
    contact_position_2 = models.CharField(max_length=60, null=True, blank=True, verbose_name='Cargo de Contacto 2')
    contact_2_phone_1 = models.CharField(max_length=15, verbose_name='Teléfono 1 de Contacto 2', blank=True, null=True)
    contact_2_phone_2 = models.CharField(max_length=15, verbose_name='Teléfono 2 de Contacto 2', blank=True, null=True)
    contact_email_2 = models.EmailField(verbose_name='Email de Contacto 2', blank=True, null=True)
    contact_3 = models.CharField(max_length=100, verbose_name='Contacto 3', null=True, blank=True)
    contact_position_3 = models.CharField(max_length=60, null=True, blank=True, verbose_name='Cargo de Contacto 3')
    contact_3_phone_1 = models.CharField(max_length=15, verbose_name='Teléfono 1 de Contacto 3', blank=True, null=True)
    contact_3_phone_2 = models.CharField(max_length=15, verbose_name='Teléfono 2 de Contacto 3', blank=True, null=True)
    contact_email_3 = models.EmailField(verbose_name='Email de Contacto 3', blank=True, null=True)
    contact_4 = models.CharField(max_length=100, verbose_name='Contacto 4', null=True, blank=True)
    contact_position_4 = models.CharField(max_length=60, null=True, blank=True, verbose_name='Cargo de Contacto 4')
    contact_4_phone_1 = models.CharField(max_length=15, verbose_name='Teléfono 1 de Contacto 4', blank=True, null=True)
    contact_4_phone_2 = models.CharField(max_length=15, verbose_name='Teléfono 2 de Contacto 4', blank=True, null=True)
    contact_email_4 = models.EmailField(verbose_name='Email de Contacto 4', blank=True, null=True)

    reason_loss = models.ForeignKey(ReasonLoss, verbose_name='Razón de Perdida', null=True, blank=True)
    manual_code = models.CharField(verbose_name='Código Manual', max_length=16, null=True, blank=True)
    installments = models.PositiveIntegerField(verbose_name='N Cuotas', validators=[MaxValueValidator(99)], null=True)
    installment_period = models.PositiveIntegerField(verbose_name="Periodo de pago de cuota", null=True)
    first_payment_month = models.PositiveIntegerField(verbose_name="Mes del primer pago", null=True)
    first_payment_year = models.PositiveIntegerField(verbose_name="Año del primer pago", null=True)
    unit_length = models.ForeignKey(UnitLength, verbose_name='Unidad de medida', null=True, blank=True)
    quantity = models.PositiveIntegerField(verbose_name='Cantidad', null=True)

    class Meta:
        verbose_name = 'SaleOpportunity'
        verbose_name_plural = 'SaleOpportunities'
        ordering = ['-created_at', ]
        default_related_name = 'saleOpportunities'

    def __str__(self):
        return '{}'.format(self.code)

    def save(self, *args, **kwargs):
        last_sale_opp = SaleOpportunity.objects.all().order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            pre_code = self.sale_anio[2:]
            if last_sale_opp:
                aux_code = last_sale_opp.sale_opp_num
                self.sale_opp_num = aux_code + 1
                self.code = '{}-{}-{}'.format(pre_code, self.user.staff.code, str(self.sale_opp_num).zfill(6))
            else:
                self.sale_opp_num = int(CORRELATIVO_INICIAL)
                self.code = '{}-{}-{}'.format(pre_code, self.user.staff.code, CORRELATIVO_INICIAL.zfill(6))

        super().save(update_fields=['sale_opp_num', 'code'])

    @property
    def late_deadline(self):
        return self.activities.filter(result__code='01').filter(end_date__lt=date.today()).count()

    @property
    def medium_deadline(self):
        return self.activities.filter(result__code='01').filter(end_date__exact=date.today()).count()

    @property
    def early_deadline(self):
        return self.activities.filter(result__code='01').filter(end_date__gt=date.today()).count()


class SaleOpportunityDetail(BaseModel):
    sale_opportunity = models.ForeignKey(SaleOpportunity, on_delete=models.CASCADE)
    month_year = models.CharField(max_length=15, null=True, blank=True, verbose_name='Mes-Año')
    quota_code = models.CharField(max_length=20)
    partial_revenue = models.FloatField(null=True, blank=True, verbose_name="Ingreso Parcial")
    is_paid = models.BooleanField(default=False, verbose_name='Pagado?')
    pay_day_relative = models.DateField(null=True, blank=True, verbose_name='Fecha de pago relativa')

    def __str__(self):
        return '{} / {}'.format(self.quota_code, self.sale_opportunity.code)

    def save(self, *args, **kwargs):
        detail_opportunities = SaleOpportunityDetail.objects.filter(sale_opportunity=self.sale_opportunity).filter(
            is_deleted=False)

        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'SaleOpportunityDetail'
        verbose_name_plural = 'SaleOpportunityDetails'
        default_related_name = 'saleopportunitydetails'


class TypeAttachedFile(BaseModel):
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    name = models.CharField(max_length=50, verbose_name='Nombre', null=True)
    description = models.CharField(max_length=200, blank=True, null=True, verbose_name='Descripcion')

    class Meta:
        verbose_name = 'Tipo Archivo Adjunto'
        verbose_name_plural = 'Tipos de Archivos Adjuntos'
        ordering = ['-created_at', ]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        last_type_file = TypeAttachedFile.objects.all().order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_type_file:
                self.code = str(int(last_type_file.code) + 1).zfill(6)
            else:
                self.code = CORRELATIVO_INICIAL.zfill(6)
        super().save(update_fields=['code'])


class AttachedFile(BaseModel):
    code = models.CharField(verbose_name='Código', max_length=10, null=True, blank=True)
    sale_opportunity = models.ForeignKey(SaleOpportunity, on_delete=models.CASCADE, verbose_name="Oportunidad de venta",
                                         null=True, blank=True)
    file_doc = models.FileField(upload_to=upload_file, null=True, blank=True)
    description = models.CharField(max_length=200, blank=True, null=True, verbose_name='Descripcion')
    type = models.ForeignKey(TypeAttachedFile, on_delete=models.CASCADE, verbose_name="Tipo", null=True, blank=True)

    class Meta:
        verbose_name = 'Archivo Adjunto'
        verbose_name_plural = 'Archivos Adjuntos'
        ordering = ['-created_at', ]
        default_related_name = 'attached_files'

    def filename(self):
        return os.path.basename(self.file_doc.name)

    def __str__(self):
        if self.file_doc and self.file_doc.file:
            return '{}-{}'.format(self.file_doc.file, self.sale_opportunity.code)
        elif self.sale_opportunity and self.sale_opportunity.code:
            return self.sale_opportunity.code
        else:
            return self.code

    def save(self, *args, **kwargs):
        last_file = AttachedFile.objects.all().order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_file:
                self.code = str(int(last_file.code) + 1).zfill(6)
            else:
                self.code = CORRELATIVO_INICIAL.zfill(6)
        super().save(update_fields=['code'])


class Result(BaseModel):
    correlative = models.PositiveIntegerField(verbose_name='Correlativo', null=True)
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    name = models.CharField(verbose_name='(*)Nombre', max_length=100)
    is_enabled = models.BooleanField(default=False, verbose_name='Habilitado')

    class Meta:
        verbose_name = 'Resultado'
        verbose_name_plural = 'Resultados'
        ordering = ['-created_at', ]

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        last_result = Result.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_result:
                aux_correlative = last_result.correlative
                self.correlative = aux_correlative + 1
                self.code = '{}'.format(str(self.correlative).zfill(2))
            else:
                self.correlative = int(CORRELATIVO_INICIAL)
                self.code = '{}'.format(CORRELATIVO_INICIAL.zfill(2))
        super().save(update_fields=['correlative', 'code'])


class TypeActivity(BaseModel):
    correlative = models.PositiveIntegerField(verbose_name='Correlativo', null=True)
    code = models.CharField(verbose_name='Código', max_length=10, null=True)
    name = models.CharField(verbose_name='Nombre', max_length=100)

    class Meta:
        verbose_name = 'TypeActivity'
        verbose_name_plural = 'TypeActivities'
        default_related_name = 'typeactivities'

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        last_type = TypeActivity.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_type:
                aux_correlative = last_type.correlative
                self.correlative = aux_correlative + 1
                self.code = str(self.correlative).zfill(2)
            else:
                self.correlative = int(CORRELATIVO_INICIAL)
                self.code = CORRELATIVO_INICIAL.zfill(2)
        super().save(update_fields=['correlative', 'code'])


class Activity(models.Model):
    MEETING = '1'
    MAIL = '2'
    CALL = '3'

    TYPES_ACTIVITY = (
        (MEETING, 'Reunión'),
        (MAIL, 'Correo'),
        (MAIL, 'Llamada'),
    )

    created_at = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False, editable=False)
    description = models.CharField(max_length=200, verbose_name='(*)Descripción')
    start_date = models.DateField(verbose_name='(*)Fecha de Inicio')
    end_date = models.DateField(verbose_name='(*)Fecha de Vencimiento')
    type = models.ForeignKey(TypeActivity,on_delete=models.CASCADE, verbose_name='(*)Tipo', null=True, blank=True)
    alert = models.BooleanField(default=False, verbose_name='Tiene Alerta')
    result = models.ForeignKey(Result, null=True, blank=True, related_name='activity', verbose_name='Resultado')
    meeting_result = models.TextField(blank=True, null=True, max_length=2000, verbose_name='(*)Resultado de Actividad')
    sale_opportunity = models.ForeignKey(SaleOpportunity, on_delete=models.CASCADE, verbose_name="Oportunidad de venta",
                                         null=True, blank=True, related_name='activities')

    class Meta:
        verbose_name = 'Actividad'
        verbose_name_plural = 'Actividades'
        ordering = ['-created_at', ]

    def __str__(self):
        return '{} {}'.format(self.start_date, self.type)


class SaleOpportunityLog(BaseModel):
    sale_opportunity = models.ForeignKey(SaleOpportunity, on_delete=models.CASCADE,
                                         related_name='sale_opportunity')
    actual_phase = models.ForeignKey(Phase, on_delete=models.CASCADE, verbose_name='Fase actual',
                                     related_name='actual_phase')
    new_phase = models.ForeignKey(Phase, on_delete=models.CASCADE, verbose_name='Fase nueva',
                                  related_name='new_phase')
