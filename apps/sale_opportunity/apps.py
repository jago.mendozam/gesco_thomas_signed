from django.apps import AppConfig


class SaleOpportunityConfig(AppConfig):
    name = 'apps.sale_opportunity'
