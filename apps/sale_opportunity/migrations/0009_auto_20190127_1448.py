# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2019-01-27 19:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sale_opportunity', '0008_auto_20190120_1655'),
    ]

    operations = [
        migrations.AlterField(
            model_name='activity',
            name='meeting_result',
            field=models.TextField(blank=True, max_length=1000, null=True, verbose_name='(*)Resultado de Actividad'),
        ),
    ]
