from __future__ import division

from datetime import datetime, date

from dateutil.relativedelta import relativedelta
from django.contrib import messages
from django.db.models import Count, Q, Sum
from django.forms import formset_factory, modelformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from django.utils.timezone import now
from django.views import View
from django.views.generic import ListView, CreateView, FormView, UpdateView, DetailView
from django_filters.views import FilterView
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_422_UNPROCESSABLE_ENTITY
from rest_framework.views import APIView

from apps.clients.models import Contact
from apps.common.constants import CODE_GANADA, MESES, CODE_NEGOCIACION
from apps.sale_opportunity.filters import AttachedFileFilter, SaleOpportunityFilter, AnioFilter
from apps.sale_opportunity.utils import valid_adjuntos, valid_opportunity, valid_create_activity, stringMonth
from apps.security.views import LoginPermissionView
from .forms import FormCreateSaleOpportunity, AttachedFileForm, FormEditSaleOpportunity, CreateActivityForm, \
    UpdateActivityForm
from .models import SaleOpportunity, Phase, SaleOpportunityDetail, AttachedFile, Activity, Result, UnitLength, \
    SaleOpportunityLog


class SaleOpportunityView(LoginPermissionView, FilterView):
    model = SaleOpportunity
    template_name = 'sale_opportunity/sale_opportunity_boards.html'
    context_object_name = 'saleOpportunities'
    filterset_class = SaleOpportunityFilter
    permission_required = 'gesco.so-dashboard'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_superuser:
            opportunities = self.filterset.qs.exclude(is_deleted=True)
        else:
            opportunities = self.filterset.qs.filter(user=self.request.user).exclude(is_deleted=True)

        context.update({
            'saleOpportunities': opportunities,
            'phases': Phase.objects.all(),
            'contacts': Contact.objects.all(),
            'units_length': UnitLength.objects.filter(is_deleted=False),
        })
        return context


class SaleOpportunityDetailView(LoginPermissionView, DetailView):
    queryset = SaleOpportunity.objects.filter(is_deleted=False)
    context_object_name = 'saleOpportunity'
    template_name = 'sale_opportunity/sale_opportunity-detail.html'
    permission_required = 'gesco.so-detail'

    def get_context_data(self, **kwargs):
        context = super(SaleOpportunityDetailView, self).get_context_data(**kwargs)
        context['attached_files'] = self.object.attached_files.filter(is_deleted=False)
        context['activities_performed'] = self.object.activities.filter(result__code='02').filter(is_deleted=False)
        context['pending_activities'] = self.object.activities.filter(result__code='01').filter(is_deleted=False)
        context['canceled_activities'] = self.object.activities.filter(result__code='03').filter(is_deleted=False)
        return context


class SaleOpportunityListView(LoginPermissionView, ListView):
    model = SaleOpportunity
    template_name = 'sale_opportunity/saleopportunity-list.html'
    context_object_name = 'oportunidades'
    permission_required = 'gesco.so-list'

    def get_queryset(self):
        if self.request.user.is_superuser:
            return SaleOpportunity.objects.filter(is_deleted=False)

        return self.request.user.sale_opportunities.filter(is_deleted=False)


class SaleOpportunityDetailAPIView(APIView):
    def get(self, request):
        try:
            oportunidad_id = self.request.GET.get('oportunidad_id')
        except ValueError:
            raise ValidationError("Oportunidad inválida")
        try:
            opportunity = get_object_or_404(SaleOpportunity, id=oportunidad_id)
        except ValueError:
            raise NotFound("Oportunidad no encontrada")
        data = []
        for detail in opportunity.saleopportunitydetails.values():
            # if detail.get("is_paid"):
            #     pagado = "SI"
            # else:
            #     pagado = "NO"
            item = ["", detail.get("quota_code"), detail.get("month_year"), detail.get("partial_revenue"),
                    detail.get("created_at").strftime('%d-%m-%Y'), detail.get("last_modified").strftime('%d-%m-%Y'),
                    detail.get("is_paid"), detail.get("pay_day_relative").strftime('%d-%m-%Y'), detail.get('id')]
            data.append(item)

        revenue_paid = 0
        revenue_pending = 0
        if opportunity.saleopportunitydetails.count() > 0:
            revenue_paid = opportunity.saleopportunitydetails.filter(is_paid=True) \
                .aggregate(Sum('partial_revenue'))['partial_revenue__sum']
            if revenue_paid is None:
                revenue_paid = 0
            revenue_pending = opportunity.saleopportunitydetails.filter(is_paid=False) \
                .aggregate(Sum('partial_revenue'))['partial_revenue__sum']

        return Response(status=HTTP_200_OK, data={
            'data': data,
            'paid': "{:,}".format(round(revenue_paid, 2)),
            'pending': "{:,}".format(round(revenue_pending, 2))})


class SaleOpportunityChangePhaseAPIView(APIView):
    def post(self, request):
        try:
            opventa_code = self.request.POST.get('opventa_code')
            fase_code = self.request.POST.get('fase_code')
        except ValueError:
            raise ValidationError("Parametros inválidos")
        try:
            opportunity = get_object_or_404(SaleOpportunity, code=opventa_code)
        except ValueError:
            raise NotFound("Oportunidad no encontrado")
        try:
            phase = get_object_or_404(Phase, code=fase_code)
        except ValueError:
            raise NotFound("Fase no encontrada")

        if not opportunity.real_revenue:
            if fase_code == CODE_GANADA:
                messages.warning(request, 'No puede cambiar una Oportunidad a Ganada si no tiene un real revenue')
                return self.get_renderer_context()
        data = []
        ganancia = round(round(opportunity.revenue, 2) * round(phase.probability, 2), 2) / 100
        data.append(ganancia)
        actual_phase = opportunity.phase
        new_phase = Phase.objects.get(code=fase_code)
        opportunity.phase = phase
        opportunity.save(update_fields=['phase'])

        if actual_phase.code != new_phase.code:
            SaleOpportunityLog.objects.create(sale_opportunity=opportunity,
                                              actual_phase=actual_phase,
                                              new_phase=new_phase)

        if fase_code == CODE_NEGOCIACION:
            real_revenue = float(self.request.POST.get('real_revenue'))
            opportunity.real_revenue = real_revenue
            opportunity.quantity = int(self.request.POST.get('quantity'))
            opportunity.unit_length = UnitLength.objects.get(pk=self.request.POST.get('unit_length'))
            opportunity.save(update_fields=['real_revenue', 'quantity', 'unit_length'])
        elif fase_code == CODE_GANADA and opportunity.real_revenue and opportunity.saleopportunitydetails.count() == 0:
            first_payment_month = int(self.request.POST.get('first_payment_month'))
            first_payment_year = int(self.request.POST.get('first_payment_year'))
            installments = int(self.request.POST.get('installments'))
            installment_period = int(self.request.POST.get('installment_period'))

            opportunity.first_payment_year = first_payment_year
            opportunity.first_payment_month = first_payment_month
            opportunity.installments = installments
            opportunity.installment_period = installment_period
            opportunity.save(update_fields=[
                'first_payment_month', 'first_payment_year', 'installments', 'installment_period'])

            first_payment_date = date(year=first_payment_year,
                                      month=first_payment_month,
                                      day=1)

            for i in range(opportunity.installments):
                if first_payment_date.day > 20:
                    next_date = first_payment_date + relativedelta(months=i * opportunity.installment_period + 1)
                else:
                    next_date = first_payment_date + relativedelta(months=i * opportunity.installment_period)

                month_year = '{}/{}'.format(stringMonth(next_date.month), next_date.year)
                SaleOpportunityDetail.objects.create(
                    created_at=now(),
                    quota_code=str(i + 1).zfill(6),
                    sale_opportunity=opportunity,
                    pay_day_relative=next_date,
                    partial_revenue=round(opportunity.real_revenue / opportunity.installments, 2),
                    is_paid=False,
                    month_year=month_year
                )
        return Response(status=HTTP_201_CREATED, data=data)


class CreateSaleOpportunity(LoginPermissionView, FormView):
    template_name = 'sale_opportunity/sale_opportunity_create.html'
    form_class = FormCreateSaleOpportunity
    success_url = reverse_lazy('sale_opportunity:saleopportunity-list')
    permission_required = 'gesco.so-create'

    def form_valid(self, form, **kwargs):
        contexto = self.get_context_data(**kwargs)
        mensaje = 'Debe ingresar el campo '
        is_valid, form = valid_opportunity(form, mensaje)
        if not is_valid:
            contexto['form'] = form
            return self.render_to_response(contexto)

        sale_opportunity = form.save(commit=False)
        if form.cleaned_data.get('phase') == 'FA-06':
            installments = form.cleaned_data.get('installments')
            installment_period = form.cleaned_data.get('installment_period')
            sale_opportunity.estimated_time = installments * installment_period
        sale_opportunity.user = self.request.user
        sale_opportunity.save()

        return redirect(self.success_url)


class RemoveOpportunityView(LoginPermissionView, View):
    permission_required = 'gesco.so-delete'
    opportunity = None

    def remover(self, request, *args, **kwargs):
        self.opportunity = SaleOpportunity.objects.get(id=self.kwargs['pk'])
        self.opportunity.is_deleted = True
        self.opportunity.save(update_fields={'is_deleted'})
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, *args, **kwargs):
        return self.remover(request, *args, **kwargs)

    def get_success_url(self):
        messages.success(self.request,
                         'Oportunidad {} eliminado con éxito.'.format(self.opportunity.code))

        return reverse('sale_opportunity:saleopportunity-list')


class UpdateOpportunityView(LoginPermissionView, UpdateView):
    model = SaleOpportunity
    template_name = 'sale_opportunity/sale_opportunity_update.html'
    form_class = FormEditSaleOpportunity
    permission_required = 'gesco.so-update'
    success_url = reverse_lazy('sale_opportunity:saleopportunity-list')
    attached_file_formset = modelformset_factory(AttachedFile, form=AttachedFileForm, max_num=3, extra=0)

    def dispatch(self, request, *args, **kwargs):
        dispatch = super().dispatch(request, *args, **kwargs)
        oportunidad = self.get_object()
        if oportunidad.phase.code == CODE_GANADA:
            messages.warning(request, '(*)Recordatorio: No puede editar una Oportunidad Ganada')
            return HttpResponseRedirect(reverse('sale_opportunity:saleopportunity-list'))
        return dispatch

    def form_valid(self, form, **kwargs):
        contexto = self.get_context_data(**kwargs)
        mensaje = 'Debe ingresar el campo '
        actual_phase = self.get_object().phase
        new_phase = form.cleaned_data.get('phase')
        opportunity = form.save(commit=False)

        is_valid, form = valid_opportunity(form, mensaje)
        if not is_valid:
            contexto['form'] = form
            return self.render_to_response(contexto)

        opportunity.last_modified = datetime.now()

        if actual_phase.code != new_phase.code:
            SaleOpportunityLog.objects.create(sale_opportunity=opportunity,
                                              actual_phase=actual_phase,
                                              new_phase=new_phase)

        if form.cleaned_data.get('phase').code == 'FA-06' and opportunity.saleopportunitydetails.count() == 0:
            installments = form.cleaned_data.get('installments')
            installment_period = form.cleaned_data.get('installment_period')
            opportunity.estimated_time = installments * installment_period
            first_payment_date = date(year=int(form.cleaned_data.get('first_payment_year')),
                                      month=form.cleaned_data.get('first_payment_month'),
                                      day=1)
            for i in range(opportunity.installments):
                if first_payment_date.day > 20:
                    next_date = first_payment_date + relativedelta(months=i * installment_period + 1)
                else:
                    next_date = first_payment_date + relativedelta(months=i * installment_period)

                month_year = '{}/{}'.format(stringMonth(next_date.month), next_date.year)
                SaleOpportunityDetail.objects.create(
                    created_at=now(),
                    quota_code=str(i + 1).zfill(6),
                    sale_opportunity=opportunity,
                    pay_day_relative=next_date,
                    partial_revenue=round(opportunity.real_revenue / opportunity.installments, 2),
                    is_paid=False,
                    month_year=month_year
                )
        opportunity.save()
        return redirect(self.success_url)


class FilterAttachedFileView(LoginPermissionView, FilterView):
    template_name = 'sale_opportunity/filter-attached-files.html'
    permission_required = 'gesco.so-files'
    queryset = AttachedFile.objects.filter(is_deleted=False)
    filterset_class = AttachedFileFilter

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        files = self.filterset.qs.exclude(is_deleted=True)

        context.update({
            'files': files,
            'fecha_hoy': now().date().isoformat()
        })
        return context


class FechaFacturacionnDetalleOportunidad(APIView):
    def post(self, request, *args, **kwargs):
        try:
            send_post = request.POST
            id_detalle = int(send_post.get('detalle_id'))
        except (ValueError, TypeError):
            raise ValidationError("Detalle inválido")
        try:
            detail = SaleOpportunityDetail.objects.get(id=id_detalle)
        except SaleOpportunityDetail.DoesNotExist:
            raise NotFound("Detalle no encontrado")
        # TODO: Manejar con Form
        detail.is_paid = send_post.get('is_paid') == 'true'
        detail.pay_day_relative = datetime.now().date()
        detail.save(update_fields=['is_paid', 'pay_day_relative'])
        opportunity = detail.sale_opportunity

        revenue_paid = opportunity.saleopportunitydetails.filter(is_paid=True) \
            .aggregate(Sum('partial_revenue'))['partial_revenue__sum']
        if revenue_paid is None:
            revenue_paid = 0
        revenue_pending = opportunity.saleopportunitydetails.filter(is_paid=False) \
            .aggregate(Sum('partial_revenue'))['partial_revenue__sum']

        return Response(status=HTTP_200_OK, data={
            'date': detail.pay_day_relative.strftime('%d-%m-%Y'),
            'paid': "{:,}".format(round(revenue_paid, 2)),
            'pending': "{:,}".format(round(revenue_pending, 2))
        })


class IngresoParcialDetalleOportunidad(APIView):
    def post(self, request, *args, **kwargs):
        try:
            send_post = request.POST
            detail_id = int(send_post.get('detalle_id'))
            new_partial_revenue = float(send_post.get('value'))
        except (ValueError, TypeError):
            raise ValidationError("Detalle inválido/revenue no es numero")
        try:
            current_detail = SaleOpportunityDetail.objects.get(id=detail_id)
        except SaleOpportunityDetail.DoesNotExist:
            raise NotFound("Detalle no encontrado")

        # recalcular partial revenue para detalles oportunidad hermanos
        opportunity = current_detail.sale_opportunity
        current_detail.partial_revenue = new_partial_revenue
        current_detail.last_modified = datetime.now()

        if opportunity.real_revenue:

            missing_income = opportunity.real_revenue

            revenue_paid = opportunity.saleopportunitydetails.filter(is_paid=True)\
                .aggregate(Sum('partial_revenue'))['partial_revenue__sum']

            if revenue_paid is not None:
                missing_income = missing_income - revenue_paid

            total_debt = missing_income

            missing_details = opportunity.saleopportunitydetails.filter(is_paid=False)\
                .exclude(id=detail_id)

            if not current_detail.is_paid:
                missing_income = missing_income - current_detail.partial_revenue

            if missing_income < 0:
                return Response(status=HTTP_422_UNPROCESSABLE_ENTITY,
                                data={'message': 'El monto ingresado debe ser menor o igual a ' + str(total_debt)})
            else:
                current_detail.save(update_fields=['partial_revenue', 'last_modified'])

            total_missing_details = missing_details.count()

            if total_missing_details > 0:
                real_revenue_para_resto = round(missing_income / total_missing_details, 2)

                for detail in missing_details:
                    detail.partial_revenue = real_revenue_para_resto
                    detail.last_modified = datetime.now()
                    detail.save(update_fields=['partial_revenue', 'last_modified'])

        return Response(status=HTTP_200_OK, data={datetime.now().strftime('%d-%m-%Y'), detail_id})


class ListActivityView(LoginPermissionView, ListView):
    model = Activity
    template_name = 'sale_opportunity/activity_list.html'
    context_object_name = 'activities'
    permission_required = 'gesco.so-activity-list'

    def get_queryset(self):
        sale_opportunity = get_object_or_404(SaleOpportunity.objects.filter(is_deleted=False), id=self.kwargs.get('pk'))
        return sale_opportunity.activities.filter(is_deleted=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                'sale_opportunity': get_object_or_404(SaleOpportunity.objects.filter(is_deleted=False),
                                                      id=self.kwargs.get('pk')),
            }
        )
        return context


class CreateActivityView(LoginPermissionView, CreateView):
    model = Activity
    form_class = CreateActivityForm
    template_name = 'sale_opportunity/activity_create.html'
    success_url = reverse_lazy('sale_opportunity:activity-list')
    permission_required = 'gesco.so-activity-create'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                'sale_opportunity': get_object_or_404(SaleOpportunity.objects.filter(is_deleted=False),
                                                      id=self.kwargs.get('pk')),
                'today': (now()).date().isoformat()
            }
        )
        return context

    def get_success_url(self):
        messages.success(self.request, 'Actividad creada con éxito.')
        return reverse('sale_opportunity:activity-list',
                       kwargs={'pk': self.get_context_data().get('sale_opportunity').id})

    def form_valid(self, form):
        context = self.get_context_data()
        mensaje = 'Debe ingresar el campo '
        is_edit = False
        is_valid, form = valid_create_activity(form, mensaje, is_edit)
        if not is_valid:
            context['form'] = form
            return self.render_to_response(context)
        self.object = form.save(commit=False)
        self.object.sale_opportunity = get_object_or_404(
            SaleOpportunity.objects.filter(is_deleted=False),
            id=self.kwargs.get('pk')
        )
        self.object.result = get_object_or_404(
            Result.objects.filter(is_deleted=False), code='01'
         )
        self.object.save()
        return super(CreateActivityView, self).form_valid(form)


class RemoveActivityView(LoginPermissionView, View):
    permission_required = 'gesco.so-activity-delete'
    activity = None

    def remover(self, request, *args, **kwargs):
        self.activity = Activity.objects.get(id=self.kwargs['pk'])
        self.activity.is_deleted = True
        self.activity.save(update_fields={'is_deleted'})
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, *args, **kwargs):
        return self.remover(request, *args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, 'Actividad eliminada con éxito.')
        return reverse('sale_opportunity:activity-list', kwargs={'pk': self.activity.sale_opportunity.id})


class UpdateActivityView(LoginPermissionView, UpdateView):
    model = Activity
    form_class = UpdateActivityForm
    template_name = 'sale_opportunity/activity_update.html'
    permission_required = 'gesco.so-activity-update'
    context_object_name = 'activity'

    #def get_object(self, queryset=None):
     #   return get_object_or_404(Activity.objects.filter(is_deleted=False), id=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                'sale_opportunity': get_object_or_404(Activity.objects.filter(is_deleted=False),
                                                      id=self.kwargs.get('pk')).sale_opportunity,
            }
        )
        return context

    def form_valid(self, form):
        context = self.get_context_data()
        mensaje = 'Debe ingresar el campo '
        is_edit = True
        is_valid, form = valid_create_activity(form, mensaje, is_edit)
        if not is_valid:
            context['form'] = form
            return self.render_to_response(context)
        activity = form.save(commit=False)
        activity.last_modified = datetime.now()
        activity.save()
        return super().form_valid(form)

    def get_success_url(self):
        messages.success(self.request, 'Actividad modificada con éxito.')
        return reverse('sale_opportunity:activity-list',
                       kwargs={'pk': self.get_object().sale_opportunity.id})


class FilterSaleManagmentView(LoginPermissionView, FilterView):
    filterset_class = AnioFilter
    template_name = 'sale_opportunity/filter-sale-managment.html'
    context_object_name = 'saleOpportunities'
    permission_required = 'gesco.so-effectiveness-indicator'
    queryset = SaleOpportunity.objects.filter(is_deleted=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Efectividad en Gestión de Ventas'
        if context['saleOpportunities'].count() > 0:
            data = []
            if self.filterset.qs:
                oportunidades = self.filterset.qs.exclude(is_deleted=True)
            else:
                oportunidades = SaleOpportunity.objects.filter(is_deleted=False)
            cantidad_oportunidades = oportunidades.count()
            cantidad_ganadas = 0
            ### totales por mes
            item = ['Número de solicitudes de cotización del mes']
            for i, j in enumerate(MESES):
                item.append(0)  # 0 .... 11
            for oportunidad in oportunidades:
                if oportunidad.saleopportunitydetails.count() > 0:
                    mes_fecha_creacion = int(oportunidad.saleopportunitydetails.order_by('created_at').first()
                                             .created_at.strftime('%m'))
                else:
                    mes_fecha_creacion = int(oportunidad.created_at.strftime('%m'))
                for i, j in enumerate(MESES):
                    if i + 1 == mes_fecha_creacion:
                        item[i + 1] = item[i + 1] + 1
            item.append(cantidad_oportunidades)
            data.append(item)
            ### totales ganadas por mes
            ganadas = oportunidades.filter(phase__code=CODE_GANADA)
            cantidad_ganadas = ganadas.count()
            item2 = ['Número de detalles de pedido de producción']
            for i, j in enumerate(MESES):
                item2.append(0)
            for oportunidad in ganadas:
                if oportunidad.saleopportunitydetails.count() > 0:
                    mes_fecha_creacion = int(oportunidad.saleopportunitydetails.order_by('created_at').first()
                                             .created_at.strftime('%m'))
                    for i, j in enumerate(MESES):
                        if i + 1 == mes_fecha_creacion:
                            item2[i + 1] = item2[i + 1] + 1
            item2.append(cantidad_ganadas)
            data.append(item2)
            ### porcentaje
            item3 = ['Indicador de efectividad de gestión de nuevas oportunidades']
            for i, j in enumerate(MESES):
                if item[i + 1] != 0:
                    porcentaje = round((item2[i + 1] / item[i + 1]) * 100, 2)
                    item3.append('{}%'.format(porcentaje))
                else:
                    item3.append("0%")
            item3.append(cantidad_oportunidades + cantidad_ganadas)
            data.append(item3)
            context['dataOportunidad'] = data
        return context


class FilterVariationNewView(LoginPermissionView, FilterView):
    filterset_class = AnioFilter
    template_name = 'sale_opportunity/filter-variation-new.html'
    context_object_name = 'saleOpportunities'
    permission_required = 'gesco.so-effectiveness-indicator'
    queryset = SaleOpportunity.objects.filter(is_deleted=False).filter(phase__code=CODE_GANADA)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Variación de nuevos negocios'
        if context['saleOpportunities'].count() > 0:
            data = []
            if self.filterset.qs:
                oportunidades = self.filterset.qs.exclude(is_deleted=True)
            else:
                oportunidades = SaleOpportunity.objects.filter(is_deleted=False).filter(phase__code=CODE_GANADA)
            cantidad_oportunidades = oportunidades.count()
            cantidad_ganadas = 0
            ### totales ganadas por mes
            item = ['Número de detalles de pedido de producción']
            for i, j in enumerate(MESES):
                item.append(0)  # 0 .... 11
            for oportunidad in oportunidades:
                if oportunidad.saleopportunitydetails.count() > 0:
                    mes_fecha_creacion = int(oportunidad.saleopportunitydetails.order_by('created_at').first()
                                             .created_at.strftime('%m'))
                    for i, j in enumerate(MESES):
                        if i + 1 == mes_fecha_creacion:
                            item[i + 1] = item[i + 1] + 1
            item.append(cantidad_oportunidades)
            ### totales ganadas por mes
            nuevos_clientes = SaleOpportunity.objects.values('client').annotate(Count('id')).order_by().filter(
                id__count=1)
            nuevos_productos = SaleOpportunity.objects.values('subproduct__product').annotate(
                Count('id')).order_by().filter(id__count=1)
            nuevos = SaleOpportunity.objects.filter(Q(client__in=[item['client'] for item in nuevos_clientes]) |
                                                    Q(subproduct__product__in=[item['subproduct__product'] for item in
                                                                               nuevos_productos]))
            cantidad_ganadas = nuevos.count()
            item2 = ['Número de detalles de pedido de producción de nuevos pedidos']
            for i, j in enumerate(MESES):
                item2.append(0)
            for oportunidad in nuevos:
                if oportunidad.saleopportunitydetails.count() > 0:
                    mes_fecha_creacion = int(oportunidad.saleopportunitydetails.order_by('created_at').first()
                                             .created_at.strftime('%m'))
                    for i, j in enumerate(MESES):
                        if i + 1 == mes_fecha_creacion:
                            item2[i + 1] = item2[i + 1] + 1
            item2.append(cantidad_ganadas)
            data.append(item2)
            data.append(item)
            ### porcentaje
            item3 = ['Indicador de variación de nuevos negocios(EFICACIA)']
            for i, j in enumerate(MESES):
                if item[i + 1] != 0:
                    porcentaje = round((item2[i + 1] / item[i + 1]) * 100, 2)
                    item3.append('{}%'.format(porcentaje))
                else:
                    item3.append("0%")
            item3.append(cantidad_oportunidades + cantidad_ganadas)
            data.append(item3)
            context['dataOportunidad'] = data
        return context


class ListAttachedFileView(LoginPermissionView, ListView):
    model = AttachedFile
    template_name = 'sale_opportunity/attached_file_list.html'
    context_object_name = 'attached_files'
    permission_required = 'gesco.so-list'

    def get_queryset(self):
        sale_opportunity = get_object_or_404(SaleOpportunity.objects.filter(is_deleted=False), id=self.kwargs.get('pk'))
        return sale_opportunity.attached_files.filter(is_deleted=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                'sale_opportunity': get_object_or_404(SaleOpportunity.objects.filter(is_deleted=False),
                                                      id=self.kwargs.get('pk')),
            }
        )
        return context


class CreateAttachedFileView(LoginPermissionView, FormView):
    form_class = AttachedFileForm
    template_name = 'sale_opportunity/attached_file_create.html'
    success_url = reverse_lazy('sale_opportunity:attached-file-list')
    permission_required = 'gesco.so-create'
    attached_file_formset = formset_factory(AttachedFileForm, max_num=3)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.method == 'GET':
            context['attachedfile_form'] = self.attached_file_formset
        context['sale_opportunity'] = get_object_or_404(SaleOpportunity.objects.filter(is_deleted=False),
                                                        id=self.kwargs.get('pk'))
        return context

    def get_success_url(self):
        messages.success(self.request, 'Archivo adjuntado con éxito.')
        return reverse('sale_opportunity:attached-file-list',
                       kwargs={'pk': self.get_context_data().get('sale_opportunity').id})

    def form_valid(self, form, **kwargs):
        contexto = self.get_context_data(**kwargs)
        sale_opportunity = get_object_or_404(
            SaleOpportunity.objects.filter(is_deleted=False),
            id=self.kwargs.get('pk')
        )
        attached_files_formset = self.attached_file_formset(self.request.POST, self.request.FILES)
        if attached_files_formset.is_valid():
            valido, attached_files_formset = valid_adjuntos(attached_files_formset)
            if attached_files_formset.is_valid():
                # To save we have loop through the formset
                for attached_file in attached_files_formset:
                    # Saving in the contacts models
                    file_doc = attached_file.cleaned_data.get('file_doc')
                    type = attached_file.cleaned_data.get('type')
                    description = attached_file.cleaned_data.get('description')
                    if file_doc and type and description:
                        AttachedFile.objects.create(
                            sale_opportunity=sale_opportunity,
                            file_doc=file_doc,
                            description=description,
                            type=type
                        )
            else:
                contexto['attachedfile_form'] = attached_files_formset
                return self.render_to_response(contexto)
        else:
            contexto['attachedfile_form'] = attached_files_formset
            return self.render_to_response(contexto)
        return super(CreateAttachedFileView, self).form_valid(form)


class RemoveAttachedFileView(LoginPermissionView, View):
    permission_required = 'gesco.so-create'
    attachedfile = None

    def remover(self, request, *args, **kwargs):
        self.attachedfile = AttachedFile.objects.get(id=self.kwargs['pk'])
        self.attachedfile.is_deleted = True
        self.attachedfile.save(update_fields={'is_deleted'})
        return HttpResponseRedirect(self.get_success_url())

    def post(self, request, *args, **kwargs):
        return self.remover(request, *args, **kwargs)

    def get_success_url(self):
        messages.success(self.request, 'Archivo adjunto eliminado con éxito.')
        return reverse('sale_opportunity:attached-file-list', kwargs={'pk': self.attachedfile.sale_opportunity.id})
