from django.core.urlresolvers import reverse, reverse_lazy
from django.conf.urls import url
from django.contrib.auth import views
from apps.sale_opportunity.views import SaleOpportunityView, SaleOpportunityListView, SaleOpportunityDetailAPIView, \
    SaleOpportunityChangePhaseAPIView, CreateSaleOpportunity, RemoveOpportunityView, UpdateOpportunityView, \
    FilterAttachedFileView, FechaFacturacionnDetalleOportunidad, ListActivityView, RemoveActivityView, \
    UpdateActivityView, CreateActivityView, IngresoParcialDetalleOportunidad, FilterSaleManagmentView, \
    ListAttachedFileView, CreateAttachedFileView, RemoveAttachedFileView, FilterVariationNewView, \
    SaleOpportunityDetailView

urlpatterns = [
    url(r'^$', SaleOpportunityListView.as_view(), name='saleopportunity-list'),
    url(r'^create/$', CreateSaleOpportunity.as_view(), name='saleopportunity-create'),
    url(r'^detail/(?P<pk>\d+)/$', SaleOpportunityDetailView.as_view(), name='saleopportunity-detail'),
    url(r'board-oportunidad-ventas/$', SaleOpportunityView.as_view(), name='ov-boards'),
    url(r'^carga/detail-op-venta/$', SaleOpportunityDetailAPIView.as_view(), name='api-detail-op-ventas'),
    url(r'^api/change-phase-op-venta/$', SaleOpportunityChangePhaseAPIView.as_view(), name='api-change-phase-op-venta'),
    url(r'^oportunidad/(?P<pk>\d+)/remover/$', RemoveOpportunityView.as_view(), name='remove-opportunity'),
    url(r'^oportunidad/(?P<pk>\d+)/editar/$', UpdateOpportunityView.as_view(), name='edit-opportunity'),
    url(r'^files/buscar/$', FilterAttachedFileView.as_view(), name='so-filter-files'),
    url(r'^detalle/oportunidad/facturado/$', FechaFacturacionnDetalleOportunidad.as_view(),
        name='detalle-fecha-facturacion'),
    url(r'^detalle/oportunidad/edit/partial-revenue/$', IngresoParcialDetalleOportunidad.as_view(),
        name='detalle-ingreso-parcial'),

    url(r'^oportunidad/(?P<pk>\d+)/actividades/$', ListActivityView.as_view(), name='activity-list'),
    url(r'^oportunidad/(?P<pk>\d+)/actividades/crear/$', CreateActivityView.as_view(), name='activity-create'),
    url(r'^oportunidad/actividad/(?P<pk>\d+)/remover/$', RemoveActivityView.as_view(), name='activity-remove'),
    url(r'^oportunidad/actividad/(?P<pk>\d+)/editar/$', UpdateActivityView.as_view(), name='activity-update'),

    url(r'^gestion/ventas/$', FilterSaleManagmentView.as_view(), name='filter-sale-managment'),
    url(r'^variacion/nuevos/$', FilterVariationNewView.as_view(), name='filter-variation-new'),

    url(r'^oportunidad/(?P<pk>\d+)/doc-adjuntos/$', ListAttachedFileView.as_view(), name='attached-file-list'),
    url(r'^oportunidad/(?P<pk>\d+)/doc-adjuntos/crear/$', CreateAttachedFileView.as_view(),
        name='attached-file-create'),
    url(r'^oportunidad/doc-adjuntos/(?P<pk>\d+)/remover/$', RemoveAttachedFileView.as_view(),
        name='attached-file-remove'),

]
