from django.contrib import admin

# Register your models here.
from apps.structure.models import SubArea, StaffPosition, Area, Staff, Position


class SubAreaInline(admin.StackedInline):
    model = SubArea
    extra = 0
    verbose_name = 'SubArea'
    verbose_name_plural = 'SubAreas'
    can_delete = True


class AreaAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'is_enable')
    search_fields = ('name', 'code')
    inlines = [SubAreaInline, ]


class StaffPositionInline(admin.StackedInline):
    model = StaffPosition
    extra = 0
    verbose_name = 'Personal Cargo'
    verbose_name_plural = 'Personal Cargos'
    can_delete = True


class StaffAdmin(admin.ModelAdmin):
    list_display = ('full_name', 'code')
    search_fields = ('full_name', 'code')
    inlines = [StaffPositionInline, ]


class PositionAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')
    search_fields = ('name', 'code')


admin.site.register(Area, AreaAdmin)
admin.site.register(Staff, StaffAdmin)
admin.site.register(Position, PositionAdmin)
