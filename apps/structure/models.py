from django.db import models

from apps.common.constants import CORRELATIVO_INICIAL
from apps.common.models import BaseModel
from apps.security.models import System


class Company(BaseModel):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    correlative = models.PositiveIntegerField(verbose_name='Correlativo', null=True)
    code = models.CharField(verbose_name='Código', max_length=10, null=True)

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'
        default_related_name = 'companies'

    def __str__(self):
        return '{}'.format(self.name)

    def save(self, *args, **kwargs):
        last_company = Company.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_company:
                aux_correlative = last_company.correlative
                self.correlative = aux_correlative + 1
                self.code = str(self.correlative).zfill(2)
            else:
                self.correlative = int(CORRELATIVO_INICIAL)
                self.code = CORRELATIVO_INICIAL.zfill(2)
        super().save(update_fields=['correlative', 'code'])


class Area(BaseModel):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    code = models.CharField(max_length=20, unique=True, editable=False, verbose_name='Código')
    initials = models.CharField(max_length=30, blank=True, null=True, verbose_name='Siglas')
    is_enable = models.BooleanField(default=True, verbose_name='Habiltada')

    class Meta:
        verbose_name = 'Área'
        verbose_name_plural = 'Áreas'
        ordering = ['-created_at', ]

    def __str__(self):
        if self.initials and self.initials != 'NULL':
            return '{}-{}'.format(self.name, self.initials)
        else:
            return self.name

    def save(self, *args, **kwargs):
        last_area = Area.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_area:
                self.code = str(int(last_area.code) + 1).zfill(4)
            else:
                self.code = '0001'
            super().save(update_fields=['code'])


class SubArea(BaseModel):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    code = models.CharField(max_length=20, unique=True, editable=False, verbose_name='Código')
    initials = models.CharField(max_length=30, blank=True, null=True, verbose_name='Siglas')
    is_enable = models.BooleanField(default=True, verbose_name='Habiltada')
    area = models.ForeignKey(Area, related_name='subareas', verbose_name='Área')

    class Meta:
        verbose_name = 'SubÁrea'
        verbose_name_plural = 'SubÁreas'
        ordering = ['-created_at', ]

    def __str__(self):
        if self.initials and self.initials != 'NULL':
            return '{}-{}'.format(self.name, self.initials)
        else:
            return self.name

    def save(self, *args, **kwargs):
        last_subarea = SubArea.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_subarea:
                self.code = str(int(last_subarea.code) + 1).zfill(4)
            else:
                self.code = '0001'
            super().save(update_fields=['code'])


class Staff(BaseModel):
    MALE = 1
    FEMALE = 2
    DNI = 1
    FOREIGNER_CARD = 2

    SEX = (
        (MALE, 'Hombre'),
        (FEMALE, 'Mujer'),
    )

    TYPE_DOCUMENT_CHOICES = (
        (DNI, 'DNI'),
        (FOREIGNER_CARD, 'CARNET EXTRANJERÍA'),
    )

    full_name = models.CharField(max_length=250, verbose_name='Nombre Completo')
    code = models.CharField(max_length=4, verbose_name='Código')
    admission_date = models.DateTimeField(blank=True, null=True, verbose_name='Fecha de ingreso')
    tipo_doc = models.PositiveSmallIntegerField(default=DNI, choices=TYPE_DOCUMENT_CHOICES)
    num_doc = models.CharField(max_length=20, null=True, blank=True, verbose_name='Número de documento')
    ruc = models.CharField(max_length=20, null=True, blank=True, verbose_name='Ruc')
    address = models.CharField(max_length=200, null=True, blank=True, verbose_name='Dirección')
    phone = models.CharField(max_length=7, null=True, blank=True, verbose_name='Teléfono Fijo')
    cell_phone = models.CharField(max_length=9, null=True, blank=True, verbose_name='Teléfono Celular')
    email = models.EmailField(max_length=50, null=True, blank=True, verbose_name='Email')
    user = models.OneToOneField('auth.User', related_name='staff', blank=True, null=True, verbose_name='Usuario')
    roles = models.ManyToManyField('security.Role', related_name='staffs', blank=True, verbose_name='Roles')

    class Meta:
        verbose_name = 'Personal'
        verbose_name_plural = 'Personal'
        ordering = ['-created_at', ]

    def __str__(self):
        return '{}'.format(self.full_name)

    def get_systems(self):
        return System.objects.filter(roles__in=self.roles.all()).distinct()


class Position(BaseModel):
    code = models.CharField(max_length=4, unique=True, editable=False, verbose_name='Código')
    name = models.CharField(max_length=50, verbose_name='Nombre')

    class Meta:
        verbose_name = 'Cargo'
        verbose_name_plural = 'Cargos'
        ordering = ['-created_at', ]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        last_position = Position.objects.order_by('pk').last()
        super().save(*args, **kwargs)
        if self.pk and not self.code:
            if last_position:
                self.code = str(int(last_position.code) + 1).zfill(4)
            else:
                self.code = '0001'
            super().save(update_fields=['code'])


class StaffPosition(BaseModel):
    staff = models.ForeignKey(Staff, on_delete=models.CASCADE, verbose_name='Personal',
                              related_name='staffpositions')
    position = models.ForeignKey(Position, on_delete=models.CASCADE, verbose_name="Cargo",
                                 related_name='staffpositions')
    area = models.ForeignKey(Area, on_delete=models.CASCADE, verbose_name="Area",
                             related_name='staffpositions')
    is_in_charge = models.BooleanField(default=False, verbose_name='Responsable del Área')
    start_date = models.DateTimeField(blank=True, null=True, verbose_name='Dia de Inicio')
    end_date = models.DateTimeField(blank=True, null=True, verbose_name='Dia Final')

    class Meta:
        verbose_name = 'Personal Cargo'
        verbose_name_plural = 'Personal Cargos'
        ordering = ['-created_at', ]
        unique_together = ('staff', 'position', 'area')

    def __str__(self):
        return '{} - {} - {}'.format(self.staff, self.position, self.area)
